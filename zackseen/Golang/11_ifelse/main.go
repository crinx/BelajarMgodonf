package main

import "fmt"

func main() {
	var a int

	//Input function
	fmt.Scanln(&a)

	//if else statement
	if a == 1 {
		fmt.Println("The number is 1")
	} else {
		fmt.Println("The number isn't 1")
	}
}
