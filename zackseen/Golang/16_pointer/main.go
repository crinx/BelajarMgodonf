package main

import "fmt"

func scale(ptr *float32, x float32) {
	*ptr = *ptr * x
}

func main() {
	var num float32
	var factor float32

	fmt.Scanln(&num)
	fmt.Scanln(&factor)

	scale(&num, factor)
	fmt.Println(num)
}
