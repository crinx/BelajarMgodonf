package main

import "fmt"

func main() {
	x := 24
	y := 15

	// Addition (Pertambahan)
	res := x + y
	fmt.Println(res)

	// Subtraction (Pengurangann)
	res = x - y
	fmt.Println(res)

	// Multiplication (Perkalian)
	res = x * y
	fmt.Println(res)

	// Division (Pembagian)
	res = x / y
	fmt.Println(res)

	// Modulus, results in the remainder of the division
	res = x % y
	fmt.Println(res)
}
