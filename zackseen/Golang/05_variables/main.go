package main

import "fmt"

//variable contain a certain value

func main() {
	var a string = "Hi" //This is a first way to declare a variable (var *variablename* *datatype*)
	b := 1              //This is a second way to declare a variable (the data type will auto)
	fmt.Println(a)
	fmt.Println(b)
}
