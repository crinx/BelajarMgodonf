package main

import "fmt"

// Rumus Menghitung Luas Persegi
func kubus() {
	fmt.Println("Menghitung Rumus Luas Persegi")
	var S int
	fmt.Print("Sisi: ")
	fmt.Scan(&S)
	fmt.Println("Luas:", S*2)
}

// Rumus Menghitung Luas Persegi Panjang
func PersegiPanjang() {
	fmt.Println("Menghitung Luas Persegi Panjang")
	var P, L int
	fmt.Print("Panjang: ")
	fmt.Scan(&P)
	fmt.Print("Lebar: ")
	fmt.Scan(&L)
	fmt.Println("Luas:", P*L)
}

func zonk() {
	fmt.Println("Rumus belum tersedia")
}

func main() {
	fmt.Println("Pilih Rumus Yang Akan Digunakan : ")
	fmt.Println("1. Rumus Persegi(Luas)")
	fmt.Println("2. Rumus Persegi Panjang(Luas)")
	fmt.Print("Rumus Mana Yang Anda Pilih : ")
	var a int
	fmt.Scan(&a)
	if a == 1 {
		kubus()
	} else if a == 2 {
		PersegiPanjang()
	} else {
		zonk()
	}
}
