package main

import "fmt"

func main() {
	//Relational operators are used to compare two values and return a bool as the result
	x := 42
	y := 8

	// equal to (Sama dengan)
	fmt.Println(x == y)

	// not equal to (Tidak sama dengan)
	fmt.Println(x != y)

	// greater than (Lebih dari)
	fmt.Println(x > y)

	// less than (Kurang dari)
	fmt.Println(x < y)
}
