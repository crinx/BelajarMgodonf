package main

import "fmt"

func main() {
	const a int = 1 //Constants are declared like variables, but with the const keyword and need to be assigned a value. Constant value can't be changed.
	fmt.Println(a)
}
