package main

import "fmt"

func main() {
	var x int

	//Input
	fmt.Scanln(&x)

	//Switch statement
	switch x {
	case 1:
		fmt.Println("One")
	case 2:
		fmt.Println("two")
	case 3:
		fmt.Println("three")
	case 4:
		fmt.Println("four")
	case 5:
		fmt.Println("five")
	case 6:
		fmt.Println("six")
	default: //if the statement is all false
		fmt.Println("The Number is ", x)
	}
}
