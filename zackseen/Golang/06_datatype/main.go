package main

import "fmt"

func main() {
	var city string = "Ponorogo"     //This is String
	var country string = "Indonesia" // Represents a string value
	var age int = 18                 // For number use Integer data type
	var height float32 = 175.6       // For Decimal number use float data type ("float32 or float64 ")
	var married bool = false         // Bool data type just contain a "True" or "False" value

	fmt.Printf("COUNTRY:")
	fmt.Println(country)
	fmt.Printf("CITY:")
	fmt.Println(city)
	fmt.Printf("AGE:")
	fmt.Println(age)
	fmt.Printf("HEIGHT:")
	fmt.Println(height)
	fmt.Printf("Married:")
	fmt.Println(married)
}
