#include <stdio.h>

int main(){
	int x;
	float num;
	char text[20];
	
	// format specifiers begin with a percent sign %
	// Blanks, tabs, and newlines are ignored
	// %[*][max_field]conversion character
	// the optional * will skip the input field
	// the optional max_width gives the maximum number
	// d decimal
	// c character
	// s string
	// f float
	// x hexadecimal
	
	scanf("%2d %*d %f %5s", &x, &num, text);
	printf("%d %f %s", x, num, text);

	return 0;
}
