#include <stdio.h>

int main(){
	int x = 1;
	printf("%d\n", x);
	x++; /* postfix */
	printf("%d\n", x);
	++x; /* prefix */
	printf("%d\n", x);
	x--;
	printf("%d\n", x);
	--x;
	printf("%d\n", x);

	return 0;
}
