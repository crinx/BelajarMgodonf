#include <stdio.h>

void ngomong();

int main(){
	for(int i = 0; i < 5; i++){
		ngomong();
	}

	return 0;
}

void ngomong(){
	static int angka = 1; /* not destroyed when a function is exited */

	printf("Hello number %d\n", angka);
	angka++;
}
