#include <stdio.h>

int main(){
	int x = 3, y = 43;
	float z = 9.0;  

	printf("%3.1f\n", (float) y / x); /* type casting */
	printf("%3.1f\n", y - z); /* auto conversion */

	return 0;
}
