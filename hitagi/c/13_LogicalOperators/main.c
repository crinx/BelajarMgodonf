#include <stdio.h>

int main(){
	unsigned int a = 3;
	signed int b = -4;
	char c = 'C';
	if(a >= 1 && b <= 0){ /* AND operator */
		printf("87\n");
	} else if(c == 'C' || c == 'c'){
		printf("C is excelent!\n");
	} else if(!(c == 'C' || c == 'c')){
		printf("C++ is excelent!\n");
	}

	return 0;
}
