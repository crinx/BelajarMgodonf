#include <stdio.h>

int main(){
	int num = 5;
	switch(num){
		case 1:
		case 2:
		case 3:
			printf("One, Two, Three\n");
			break;
		case 4:
		case 5:
		case 6:
			printf("Four, Five, Six\n");
			break;
		default:
			printf("Greater than six\n");
	}

	return 0;
}
