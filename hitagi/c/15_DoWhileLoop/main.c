#include <stdio.h>

int main(){
	int count = 1;

	do {
		printf(" %d,", count);
		count++; /* exit condition */
	} while(count < 10);

	do {
		printf(" %d,", count);
		if(count > 7){
			count--;
			continue;
		} else {
			break;
		}
	} while(count > 0);
	
	return 0;
}
