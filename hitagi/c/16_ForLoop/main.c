#include <stdio.h>

int main(){
	int max = 10, min = 5, average = 20;

	for(int i = 0; i <= max; i++, max--){
		printf(" %d,", i);
	}
	printf("\n");

	for(; min < average; min++){ /* skip initvalue */
		printf(" %d,", average);
	}

	return 0;
}
