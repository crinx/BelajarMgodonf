#include <stdio.h>

int main(){
	int a[2][3] = { /* two dimensions array (2 x 3) */
		{3, 2, 4},
		{4, 5, 6}
	};
	
	printf("%d\n", a[0][0]); /* 3 */
	printf("%d\n", a[1][2]); /* 6 */

	return 0;
}
