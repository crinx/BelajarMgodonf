#include <stdio.h>

int main(){
	char a = getchar();
	int b;

	printf("You entered: ");
	putchar(a); // output a single character

	scanf("%d", &b);
	printf("\nYou entered: %d\n", b);

	return 0;
}
