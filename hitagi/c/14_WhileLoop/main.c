#include <stdio.h>

int main(){
	int count = 1;

	while(count < 10){
		printf("%d\n", count);
		count++; /* exit condition */
	}

	return 0;
}
