#include <stdio.h>

int main(){
	/* pointer is a variable, with the address of another variable as its value */

	int a = 10;
	/* asterisk operator is indicating of pointer */
	int *b = NULL;
	int **c = NULL;
	/* ampersand operator is address of variable */
	b = &a;
	c = &b;

	/* print address to hex format */
	printf("The address of a is %x\n", &a);
	printf("b contains address %x\n", b);
	printf("c contains address %x\n", *c);
	printf("The value of a is %d\n", a);
	printf("b is pointing to this value %d\n", *b);
	printf("c is pointing to this value %d\n", **c);

	return 0;
}
