#include <stdio.h>

int main(){
	printf("the three has %d apples\n", 22);
	printf("\"Hello World!\"\n");
	printf("Phi: %-2.4f", 3.142341);
	// escape sequences begin with a backslash \:
	// \n new line
	// \\ backslash
	// \b backspace
	// \' single quote
	// \" double quote
	// %[-][width].[precision]conversion character
	// - specifies left alignment of the data in the string
	// width gives the minimum number of characters for the data
	// period . separates the width from the precision
	// precision gives the number of decimal places for numeric data
	// to print the % symbol, use %% in the format string

	return 0;
}
