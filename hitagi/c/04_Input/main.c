#include <stdio.h>

int main(){
	char a = getchar(); // Returns the value of the next single character input
	int b, c, d;

	printf("You entered: %c \n", a);

	scanf("%d", &b); // scans input that matches format specifiers
	printf("You entered: %d \n", b);

	printf("Enter two numbers:");
	scanf("%d %d", &c, &d);
	printf("\nSum: %d \n", c + d);	

	return 0;
}
