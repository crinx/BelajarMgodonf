#include <stdio.h>
/* prototype */
int square(int x, int y);

int main(){
	int x = 5, y = 4;
	printf("%d\n",square(x, y)); /* call the function */

	return 0;
}

/* definition */
int square(int x, int y){
	return x * y; /* return statement */
}
