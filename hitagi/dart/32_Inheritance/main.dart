class Binatang {
	String _nama = "";
	int _usia = 0;
	double _bobot = 0;
  
  Binatang(this._nama, this._usia, this._bobot);
  
  String get nama => _nama;
  double get bobot => _bobot;

	void makan()
	{
		print("$_nama sedang makan");
		_bobot = _bobot + 0.1;
	}
}

class Kucing extends Binatang {
	String warnaBulu = "";
	Kucing(String nama, int usia, double bobot, String warnaBulu) : super(nama,usia,bobot);

	void berjalan()
	{
		print("$nama sedang berjalan");
	}
}

void main()
{
	var kucingSaya = Kucing("Slamet", 2, 3.4,"Putih");
	kucingSaya.berjalan();
	kucingSaya.makan();
	print(kucingSaya.bobot);
}
