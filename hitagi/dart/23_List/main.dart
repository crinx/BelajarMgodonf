void main()
{
	// eksplisit list
	List<int> listAngka = [0,1,2,3,4,5,6];
	List<String> listHuruf = ["b","c","d","e"];

	// tidak eksplisit
	var susunanAngka = [0,1,2,3,4,5,6];
	var susunanHuruf = ["a","b","c","d,","e"];

	// list dinamis
	var dinamisList = [0,"Makan", false];

	// mengambil data dalam list sesuai index
	print(dinamisList[1]);

	// mengambil data dalam list dengan looping
	for(int i = 0; i < listHuruf.length; i++)
	{
		print(listHuruf[i]);	
	}

	// mengambil data dalam list dengan forEach
	//listHuruf.forEach((s)) => print((s));
	
	// menambahkan nilai kedalam list
	listHuruf.add("f");

	// menambahkan nilai kedalam list sesuai index
	listHuruf.insert(0,"a"); /* atau */ listHuruf[0] = "a";

	// menghapus list
	listHuruf.remove("f"); // menghapus data list sesuai dengan data yang sudah diketahui
	listHuruf.removeAt(0); // menghapus data list sesuai index
	listHuruf.removeLast(); // menghapus data list terakhir
	listHuruf.removeRange(0,1); // menghapus data list sesuai dengan jangkauan index
}
