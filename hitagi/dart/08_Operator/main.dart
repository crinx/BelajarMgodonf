void main()
{
	print(1 + 2); // penjumlahan
	print(1 - 2); // pengurangan
	print(1 * 2); // perkalian
	print(1 / 2); // pembagian
	print(1 ~/ 2); // pembagian, mengembalikan nilai int
	print(1 & 2); // modulus

	int a = 1, b = 9;
	a++; // increment
	b--; // decrement

	// || NOT
	// && DAN
	// ! NOT
}
