void main()
{
	Set<int> setAngka1 = {9,1,5};
	// secara otomatis akan membuang angka yang sama
	Set<int> setAngka2 = new Set.from({1,4,5,9,2,6});
	
	print(setAngka2);

	// menambahkan data kedalam set
	// tunggal
	setAngka1.add(8);
	// beberapa data
	setAngka1.addAll({8,4,3});

	// menghapus data dalam set bukan index
	setAngka1.remove(1);

	// menampilkan data set pada suatu index
	print(setAngka1.elementAt(2));
}
