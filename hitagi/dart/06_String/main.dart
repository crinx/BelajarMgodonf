void main()
{
	String dobelPetik = "tanda petik ganda";
	String singlePetik = "tanda petik tunggal";

	print("Hari jum\'at adalah hari yang mulia."); 
	//backslash adalah tanda untuk memberi tahu intepreter agar tidak menganggap tanda petik tunggal tersebut sebagai pembatas string.
	print(r"Selamat hari jum\'at");
	// huruf r sebelum string memberi tahu intepreter agar menampilkan string secara raw atau tidak menggunakan string interpolation.
	String name = "Alif";
	print("Hello $name, nice to meet you");
	print('1 + 1 = ${ 1 + 1 }');
}
