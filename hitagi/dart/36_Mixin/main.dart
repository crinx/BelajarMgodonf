mixin berjalan{
    void jalan()
    {
        print("sedang berjalan");
    }
}

class Binatang{
    String nama = "";
    int umur = 0;
    double bobot = 0.0;
    
    Binatang(this.nama,this.umur,this.bobot);
}

class Mamalia extends Binatang{
    Sting berkembangBiak = "menyusui";
    Mamalia(String nama, int umur, double bobot) : super(nama,umur,bobot);
}

class Kucing extends Mamalia with berjalan{
    Kucing(String nama, int umur, double bobot) : super(nama,umur,bobot);

}


void main()
{
    var kucingSaya = Kucing("Slamet", 3, 3.4);
    print(kucingSaya.berkembangBiak);
    print("$kucingSaya.nama $kucingSaya.jalan");
}
