extension Sorting on List<int> {
    List<int> sortAsc()
    {
        var list = this;
        var panjang = this.length;
      
        for (int i = 0; i < panjang - 1; i++)
        {
            int min = i;
            for (int j = i + 1; j < panjang; j++)
            {
                if (list[j] < list[min])
                {
                    min = j;
                }
            }
            
            int tmp = list[min];
            list[min] = list[i];
            list[i] = tmp;
        }
        
        return list;
    }
}
