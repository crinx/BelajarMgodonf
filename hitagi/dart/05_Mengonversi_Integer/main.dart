void main()
{
	// string -> int
	var eleven = int.parse('11');

	// string -> double
	var elevenPointTwo = double.parse('11.2');

	// int -> string
	var elevenAsString = 11.toString();

	// double -> string
	var piAsString = 3.14159.toStringAsFixed(2); // string piAsString = '3.14'
}
