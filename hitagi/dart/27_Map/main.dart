void main()
{
	var a = {
		// key : data
		"Jakarta : Indonesia",
		"Malaysia : Kuala Lumpur",
		"Jawa Timur : Surabaya"
	};
	
	// mengambil data dengan key
	print(a['Jakarta']);

	// menampilkan seluruh key
	print(a.keys);

	// menambahkan key data
	a["New Delhi"] = "India";
	print(a);

}
