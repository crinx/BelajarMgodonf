void main()
{
	var makanan = ["Nila bakar", "Pecel lele", "Nasi goreng"];
	var minuman = ["Es teh", "Kopi hitam", "Susu jahe"];

	// menggabungkan nilai list
	var menu = [ makanan, minuman ];
	print(menu);

	// menggabungkan nilai list tanpa pemisah
	var Amenu = [...makanan, ...minuman];
	print(Amenu);

	// menggabungkan list yang berisi null
	var list1;
	var list2 = [0, ...?list1];
	print(list2);
}
