void main()
{
	try{
		var a = 9;
		var b = 0;
		print(a ~/ b);
	} on IntegerDivisionByZeroException {
		print("can not divide by zero");
	}
}
