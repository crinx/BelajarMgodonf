void main()
{
	try{
		var a = 9;
		var b = 0;
		print(a ~/ b);
	} catch(c,d){
		print("Exception happened : $c");
		print("Stack Trace : $d");
	} finally{
		print("This line still executed");
	}
}
