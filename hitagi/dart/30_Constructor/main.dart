class Binatang {
	String nama = "";
	int umur = 0;
	double bobot = 0;
}

class Makanan {
	Makanan.jeneng(String nama){
		nama = "";
	}
	Makanan.unsur(String bahan){
		bahan = "";
	}
	Makanan.roso(String rasa){
		rasa = "";
	}
}

void main()
{
	var kucingSaya = Binatang();
	kucingSaya.nama = "Slamet";
	kucingSaya.umur = 2;
	kucingSaya.bobot = 3.4;
	print(kucingSaya.nama);
	print(kucingSaya.umur);
	print(kucingSaya.bobot);

	String nama = "Klepon";
	var jenengPanganan = Makanan.jeneng(nama);
	print(jenengPanganan);
}
