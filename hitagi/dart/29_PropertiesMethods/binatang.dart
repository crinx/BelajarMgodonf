class binatang {
	String _nama = '';
	int _umur = 0;
	double _bobot = 0;

	binatang(this._nama, this._umur, this._bobot);

	// setter
	set nama(String value){
		_nama = value;
	}

	// getter
	double get bobot => _bobot;

	void makan()
	{
		print('$_nama sedang makan');
		_bobot = _bobot + 0.2;
	}

	void tidur()
	{
		print('$_nama sedang tidur');
	}

	void buangHajat()
	{
		print('$_nama sedang buang hajat');
		_bobot = _bobot - 0.1;
	}
}
