void main()
{
	// tidak eksplisit
	var nama = 'Alif Nurcahyo'; // string
	var usia = 18; // integers

	// eksplisit
	String name = 'Kurumi Tokisaki';
	int age = 18;

	// dynamic 'harus dideklarasikan sebelum diisi sebuah nilai'
	var x;  // dynamic
	x = 7;
	x = 'tujuh';
	print(x);
}
