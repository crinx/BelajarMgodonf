int fibonacci(n)
{
    if((n == 0) || (n == 1))
    {
        print("Fibonacci : $n\n");
        return n;
    }else {
        return fibonacci(n - 1) + fibonacci(n - 2);
    }
}

void main()
{
    int n = 9;
    print(fibonacci(n));
}
