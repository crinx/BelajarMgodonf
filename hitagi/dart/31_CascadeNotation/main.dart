class Binatang {
	String nama = "";
	int usia = 0;
	double bobot = 0.0;

	Binatang(this.nama,this.usia,this.bobot);
	
	void makan()
	{
		print("$nama sedang makan");
		bobot = bobot + 0.1;
	}
}

void main()
{
	// ditulis dalam operator dua titik (..) atau (..?) untuk mempercepat langkah membuat variabel sementara
	var kucingSaya = Binatang("",2,3.4)
	..nama = "Slamet"
	..makan();
}
