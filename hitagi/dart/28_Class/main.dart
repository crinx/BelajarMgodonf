class binatang
{
	String nama;
	int umur;
	double bobot;

	binatang(this.nama, this.umur, this.bobot);

	void makan()
	{
		print("$nama sedang makan.");
		bobot = bobot + 0.2;
	}

	void tidur()
	{
		print("$nama sedang tidur.");
	}

	void buangHajat()
	{
		print("$nama sedang buang  hajat.");
		bobot = bobot - 0.1;
	}
}


void main()
{

var kucingSaya = binatang("Slamet", 1, 2.9);
kucingSaya.makan();
kucingSaya.tidur();
kucingSaya.buangHajat();
print(kucingSaya.bobot);

}

