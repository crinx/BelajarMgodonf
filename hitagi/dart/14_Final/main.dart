import "dart:io";

void main()
{
	// value dari variabel final diubah ketika runtime dan tidak bisa diubah lagi

	final a = stdin.readLineSync();
	final String b = stdin.readLineSync()!;

	stdout.write("Hallo $a $b!");
}
