const routes = [
  // detailed notation.
  {
    method: 'GET',
    path: '/about',
    // request from client, response to client.
    handler: (request, h) => {
      // set response message.
      const response = h.response('success');
      // set header type to plain text.
      response.type('text/plain');
      // and powered by some-value :v.
      response.header('X-custom', 'some-value');
      // successful request.
      response.code(203);
      // return all response value.
      return response;
    }
  },
  // chained notation.
  {
    method: 'GET',
    path: '/ingfo',
    handler: (request, h) => {
      // return all response value.
      return h.response('success')
        // return header type to plain text.
        .type('text/plain')
        // return powered by some-value :v.
        .header('X-custom', 'some-value')
        // successful request.
        .code(202);
    }
  }
];

module.exports = routes;
