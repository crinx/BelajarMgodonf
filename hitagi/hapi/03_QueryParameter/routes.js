const routes = [
  {
    method: 'GET',
    path: '/',
    handler: (request, h) => {
      return 'Homepage';
    },
  },
  {
    method: '*',
    path: '/',
    handler: (request, h) => {
      return 'Halaman ini tidak dapat diakses oleh method tersebut';
    },
  },
  {
    method: 'GET',
    path: '/about',
    handler: (request, h) => {
      return 'About page';
    },
  },
  {
    method: '*',
    path: '/about',
    handler: (request, h) => {
      return 'Halaman ini tidak dapat diakses oleh method';
    },
  },
  {
    method: 'GET',
    path: '/users/{username?}',
    handler: (request, h) => {
      const { username = "pipel" } = request.params;
      // request query
      const { lang } = request.query;

      // with lang query.
      if (lang === 'id') {
        return `Hai, ${username}`
      }
      
      // without lang query.
      return `Hello ${username}`
    },
  },
  {
    method: '*',
    path: '/{any*}',
    handler: (request, h) => {
      return 'Halaman tidak ditemukan';
    },
  },
];

module.exports = routes;
