// import from third-party module Hapi.
const Hapi = require('@hapi/hapi');
// import from routes.js
const routes = require('./routes');

// using asyncronous method.
const init = async () => {
  // create server.
  const server = Hapi.server({
    // port.
    port: 5000,
    // host.
    host: 'localhost',
  });

  // route server.
  server.route(routes);
  
  await server.start();
  console.log(`Server berjalan pada ${server.info.uri}`);
}

// call asyncronous method was created.
init();
