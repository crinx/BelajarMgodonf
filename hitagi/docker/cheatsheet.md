# Docker Cheat Sheet
`docker <subcommand> <option> <object>`

## Download images from docker hub repository
`docker pull <name of user/name of image:tag>`

## Docker Images
In Docker, everything is based on images. An images is a combination of a file system and parameters.

### run the images
if the image is not available locally, it will be automatically downloaded via the repository.

`docker run <option> <name of image>`

### display docker images
`docker images <option>`

### removing docker images
`docke rmi <image id>`

### docker inspect
used see the details of an image or container.

`docker inspect <name of image or container>`

## Docker Containers
container are intances of docker images.

### listing of containers
`docker ps <option>`

### docker history
see all the commands that were run with an image via a container.

`docker history <image id>`

### docker top
see the top processes whitin a container.

`docker top <container id>`

### docker stop
used to stop a running container.

`docker stop <container id>`

### docker rm
used to delete a container.

`docker rm <container id>`

### docker stats
used to provide the statistics of a running container.

`docker stats <containter id>`

### docker attach
used to attach to a running container.

`docker attach <container id>`

### docker pause
used to pause the processes in a running container.

`docker pause <container id>`

### docker unpause
used to unpause the processes in a running container.

`docker unpause <container id>`

### docker kill
used to kill the processes in a running container.

`docker kill <container id>`


