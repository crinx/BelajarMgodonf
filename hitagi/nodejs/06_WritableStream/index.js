// import from core module 'fs'.
const fs = require('fs');

// path output file.
const writableStream = fs.createWriteStream('./notes.txt');

// write data to a file.
writableStream.write('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\n');
writableStream.write('Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.\n');
writableStream.write('Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.\n');

// end of file.
writableStream.end('Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.');
