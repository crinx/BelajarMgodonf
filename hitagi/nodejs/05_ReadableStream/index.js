// import  from core module 'fs'.
const fs = require('fs');

// read data from file.
const readableStream = fs.createReadStream('./notes.txt', {
  // 10 bytes to read 10 character.
  highWaterMark: 10
});

// print data to stdout with error handling.
readableStream.on('readable', () => {
  try {
    process.stdout.write(`[${readableStream.read()}]`);
  } catch (error) {
    console.log('fail to read file.');
  }
});

// end of file.
readableStream.on('end', () => {
  console.log('Done.');
});
