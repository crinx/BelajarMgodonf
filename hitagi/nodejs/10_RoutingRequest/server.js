// import from core module 'http'.
const http = require('http');

// request from client, response to client.
const requestListener = (request, response) => {
  // set header type.
  response.setHeader('Content-Type', 'text/html');
  // return status code.
  response.statusCode = 200;

  // check method request.
  const { method, url } = request;

  if (url == '/') {
    
  } else if (url == '/about') {
    if (method == 'GET'){
      response.end('<h1>Halo! Ini adalah halaman about</h1>');
    }

    if (method == 'POST') {
      // save body data.
      let body = [];
    
      // stream based data saved to body variable.
      request.on('data', (chunk) => {
        // data in array.
        body.push(chunk);
      });

      request.on('end', () => {
        // convert data array to single line string.
        body = Buffer.concat(body).toString();
        // process input json model to human readable.
        const { name } = JSON.parse(body);
        response.end(`<h1>Hai, ${name}!</h1>`);
      });
    }

    if (method == 'PUT') {
      response.end('<h1>Halaman tidak dapat diakses menggunakan PUT request</h1>');
    }

    if (method == 'DELETE') {
      response.end('<h1>Halaman tidak dapat diakses menggunakan DELETE request</h1>');
    }
    
  } else {
    response.end('<h1>Halaman tidak ditemukan!</h1>')
  }
};

const server = http.createServer(requestListener);

const port = 5000;
const host = 'localhost';

server.listen(port, host, () => {
  console.log(`Server berjalan pada http://${host}:${port}`);
});
