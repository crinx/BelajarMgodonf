// import  from core module 'fs'.
const fs = require('fs');

// callback function.
const fileReadCallBack = (error, data) => {
  if(error){
    console.log('Gagal membaca file.');
    return;
  }
  console.log(data);
};

// asynchronous.
fs.readFile('notes.txt', 'UTF-8', fileReadCallBack);

// synchronous.
const data = fs.readFileSync('notes.txt', 'UTF-8');
console.log(data);
