// import from third-party module Hapi.
const Hapi = require('@hapi/hapi');
// import routes configuration.
const routes = require('./routes');

const init = async () => {
  // create server.
  const server = Hapi.server({
    // port.
    port: 5000,
    // host.
    host: 'localhost',
    // routes CORS (Cross-origin Resource Sharing).
    routes: {
      cors: {
        // can accessed from any origin client.
        origin: ['*'],
      },
    },
  });

  // use routes configuration.
  server.route(routes);

  await server.start();
  console.log(`Server berjalan pada ${server.info.uri}`);
};

init();
