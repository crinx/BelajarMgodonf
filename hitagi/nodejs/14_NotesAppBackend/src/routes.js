// import handler statement from handler.js
const {
  addNoteHandler,
  getAllNotesHandler,
  getNoteByIdHandler,
  editNoteByIdHandler,
  deleteNoteByIdHandler,
} = require('./handler');

const routes = [
  // add note handler configuration.
  {
    method: 'POST',
    path: '/notes',
    handler: addNoteHandler,
  },
  // get all notes handler configuration.
  {
    method: 'GET',
    path: '/notes',
    handler: getAllNotesHandler,
  },
  // get notes by id.
  {
    method: 'GET',
    path: '/notes/{id}',
    handler: getNoteByIdHandler,
  },
  // update note.
  {
    method: 'PUT',
    path: '/notes/{id}',
    handler: editNoteByIdHandler,
  },
  // delete note.
  {
    method: 'DELETE',
    path: '/notes/{id}',
    handler: deleteNoteByIdHandler,
  },
];

// export routes as module.
module.exports = routes;
