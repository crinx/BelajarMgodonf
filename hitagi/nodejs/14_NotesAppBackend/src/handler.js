// import from third-party module nanoid.
const { nanoid } = require('nanoid');
// import database structure.
const notes = require('./notes');

// create handler.
const addNoteHandler = (request, h) => {
  // notes data as JSON.
  const { title, tags, body } = request.payload;

  // create special id number with nanoid.
  const id = nanoid(16);
  // to add and update notes.
  const createAt = new Date().toISOString();
  const updateAt = createAt;

  const newNote = {
    title, tags, body, id, createAt, updateAt,
  };

  // insert new data to database.
  notes.push(newNote);

  // to confirm whatever data added or not.
  const isSuccess = notes.filter((note) => note.id === id).length > 0;

  // and to return response to client.
  if (isSuccess) {
    const response = h.response({
      status: 'success',
      message: 'Catatan berhasil ditambahkan',
      data: {
        noteId: id,
      },
    });
    response.code(201);
    return response;
  }

  const response = h.response({
    status: 'fail',
    message: 'Catatan gagal ditambahkan',
  });
  response.code(500);
  return response;
};

const getAllNotesHandler = () => ({
  status: 'success',
  data: {
    notes,
  },
});

// get note by id handler configuration.
const getNoteByIdHandler = (request, h) => {
  // get id from path.
  const { id } = request.params;

  // to get id value, use array.filter() to get object.
  const note = notes.filter((n) => n.id === id)[0];

  // confirm data was not undefined value before return them.
  if (note !== undefined) {
    return {
      status: 'success',
      data: {
        note,
      },
    };
  }

  const response = h.response({
    status: 'fail',
    message: 'Catatan tidak ditemukan',
  });
  response.code(404);
  return response;
};

// update note handler configuration.
const editNoteByIdHandler = (request, h) => {
  // get id from route params.
  const { id } = request.params;

  // get data from client request.
  const { title, tags, body } = request.payload;
  // to add and update note.
  const updateAt = new Date().toISOString();

  // get array index from note by id.
  const index = notes.findIndex((note) => note.id === id);

  // to handle data when it is at index -1.
  if (index !== -1) {
    notes[index] = {
      ...notes[index],
      title,
      tags,
      body,
      updateAt,
    };

    const response = h.response({
      status: 'success',
      message: 'Catatan berhasil diperbarui',
    });
    response.code(200);
    return response;
  }

  const response = h.response({
    status: 'fail',
    message: 'Catatan tidak ditemukan',
  });
  response.code(404);
  return response;
};

// delete note handler configuration.
const deleteNoteByIdHandler = (request, h) => {
  const { id } = request.params;

  const index = notes.findIndex((note) => note.id === id);

  // method to delete data from array.
  if (index !== -1) {
    notes.splice(index, 1);
    const response = h.response({
      status: 'success',
      message: 'Catatan berhasil dihapus',
    });
    response.code(200);
    return response;
  }

  const response = h.response({
    status: 'fail',
    message: 'Catatan gagal dihapus, id tidak ditemukan',
  });
  response.code(404);
  return response;
};

// export as module.
module.exports = {
  addNoteHandler,
  getAllNotesHandler,
  getNoteByIdHandler,
  editNoteByIdHandler,
  deleteNoteByIdHandler,
};
