// import from core module 'http'.
const http = require('http');

// request from client, response to client.
const requestListener = (request, response) => {
  // set header type to json.
  response.setHeader('Content-Type', 'application/json');
  // and powered by nodejs.
  response.setHeader('X-Powered-By', 'NodeJS');

  // check method request.
  const { method, url } = request;

  if (url == '/') {
    if (method == 'GET') {
      // successful request.
      response.statusCode = 200;
      response.end('<h1>Ini adalah homepage</h1>');
    } else {
      // client error.
      response.statusCode = 400;
      response.end(`<h1>Halaman tidak dapat diakses dengan ${method} request</h1>`);
    }
  } else if (url == '/about') {
    if (method == 'GET'){
      response.statusCode = 200;
      response.end('<h1>Halo! Ini adalah halaman about</h1>');
    } else if (method == 'POST') {
      // save body data.
      let body = [];
    
      // stream based data saved to body variable.
      request.on('data', (chunk) => {
        // data in array.
        body.push(chunk);
      });

      request.on('end', () => {
        response.statusCode = 200;
        // convert data array to single line string.
        body = Buffer.concat(body).toString();
        // process input json model to human readable.
        const { name } = JSON.parse(body);
        response.end(`<h1>Hai, ${name}!</h1>`);
      });
    } else {
      response.statusCode = 400;
      response.end(`<h1>Halaman tidak dapat diakses menggunakan ${method} request</h1>`);
    }

  } else {
    // client error.
    response.statusCode = 404;
    response.end('<h1>Halaman tidak ditemukan!</h1>')
  }
};

const server = http.createServer(requestListener);

const port = 5000;
const host = 'localhost';

server.listen(port, host, () => {
  console.log(`Server berjalan pada http://${host}:${port}`);
});
