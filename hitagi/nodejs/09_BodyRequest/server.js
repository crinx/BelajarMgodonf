// import from core module 'http'.
const http = require('http');

// request from client, response to client.
const requestListener = (request, response) => {
  // set header type.
  response.setHeader('Content-Type', 'text/html');
  // return status code.
  response.statusCode = 200;

  // check method request.
  const { method } = request;

  if (method == 'GET') {
    response.end('<h1>Hello!</h1>')
  }

  if (method == 'POST') {
    // save body data.
    let body = [];
    
    // stream based data saved to body variable.
    request.on('data', (chunk) => {
      // data in array.
      body.push(chunk);
    });

    request.on('end', () => {
      // convert data array to single line string.
      body = Buffer.concat(body).toString();
      // process input json model to human readable.
      const { name } = JSON.parse(body);
      response.end(`<h1>Hai, ${name}!</h1>`);
    });
  }
};

const server = http.createServer(requestListener);

const port = 5000;
const host = 'localhost';

server.listen(port, host, () => {
  console.log(`Server berjalan pada http://${host}:${port}`);
});
