// import from core module events.
const { EventEmitter } = require('events');

// create new class from EventEmitter.
const myEmitter = new EventEmitter();

// function to handle special events.
const birthdayEventListener = ({name}) => {
  console.log(`Happy birthday ${name}!`);
}

// act when an incident occurs.
myEmitter.on('birthday', birthdayEventListener);

// trigger the birthday event to happen.
myEmitter.emit('birthday', { name: 'Hitagi' });
