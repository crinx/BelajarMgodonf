// Modularization
// one file only bears one responsibility only.

// * local module: the locally generated module is located in the nodejs.gy project.
// * core module: located in the lib directories.
// * third party module: installed via npm and located in the node_modules directory.


const Tiger = require('././tiger.js')
const Wolf = require('././wolf.js');

const fighting = (tiger, wolf) => {
  if(tiger.strength > wolf.strength){
    tiger.growl();
    return;
  }

  if(tiger.strength < wolf.strength){
    wolf.howl();
    return;
  }

  console.log("Tiger and Wolf have same strength");
}

const tiger = new Tiger();
const wolf = new Wolf();

fighting(tiger, wolf);
