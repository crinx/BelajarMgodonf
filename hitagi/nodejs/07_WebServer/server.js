// import from core module 'http'.
const http = require('http');

// request from client, response to client.
const requestListener = (request, response) => {
  // set header type.
  response.setHeader('Content-Type', 'text/html');
  
  // return status code.
  response.statusCode = 200;
  // return html value.
  response.end('<h1>Hello World!</h1>');
};

// create server.
const server = http.createServer(requestListener);

// port
const port = 5000;
// host
const host = 'localhost';

// trigger when client have request.
server.listen(port, host, () => {
  console.log(`Server berjalan pada http://${host}:${port}`);
})
