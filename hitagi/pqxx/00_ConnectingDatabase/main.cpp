#include <exception>
#include <iostream>
#include <pqxx/pqxx>

int main(){
	try {
		pqxx::connection C("dbname = dvdrental user = postgres password = productive host = localhost port = 5432"); // used to connect database.
		if(C.is_open()){ // is a public method of connection object and returns boolean value.
			std::cout << "Openned database successfully: " << C.dbname() << std::endl;
		} else {
			std::cout << "Can't open database" << std::endl;
			return 1;
		}

		C.disconnect(); // used to disconnect an opened database connection.
	} catch(const std::exception &e){ // to catch error 
		std::cerr << e.what() << std::endl; // to display error
		return 1;
	}

	return 0;
}
