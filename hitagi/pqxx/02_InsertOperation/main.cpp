#include <exception>
#include <iostream>
#include <pqxx/pqxx>
#include <pqxx/connection.hxx>

int main(){
	char *sql;

	try {
		pqxx::connection C("dbname = databaseGwejh user = postgres password = productive \
		hostaddr = 127.0.0.1 port = 5432");
		if(C.is_open()){
			std::cout << "Opened database successfully: " << C.dbname() << std::endl;
		} else {
			std::cout << "Can't open database" << std::endl;
			return 1;
		}
			
			/* Create a sql statement */
		sql = "INSERT INTO COMPANY (ID, NAME, AGE, ADDRESS, SALARY) " \
		       "VALUES (1, 'Paul', 32, 'Calivornia', 20000.00); " \
		       "INSERT INTO COMPANY (ID, NAME, AGE, ADDRESS, SALARY) "\
		       "VALUES (2, 'Allen', 25, 'Texas', 15000.00); " \
		       "INSERT INTO COMPANY (ID, NAME, AGE, ADDRESS, SALARY) "\
		       "VALUES (3, 'Teddy', 23, 'Norway', 20000.00); " \
		       "INSERT INTO COMPANY (ID, NAME, AGE, ADDRESS, SALARY)" \
		       "VALUES (4, 'Mark', 25, 'Rich-Mond', 65000.00); ";

			/* Create a transactional object */
		pqxx::work W(C);
			
			/* execute sql query */
		W.exec(sql);
		W.commit();
		std::cout << "Record created successfully" << std::endl;

		C.disconnect();
	} catch(const std::exception &e){
		std::cerr << e.what() << std::endl;
	}

	return 0;
}
