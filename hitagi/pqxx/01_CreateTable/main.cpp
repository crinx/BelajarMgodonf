#include <exception>
#include <iostream>
#include <pqxx/pqxx>
#include <pqxx/transaction.hxx>

int main(){
	char *sql;

	try {
		pqxx::connection C("dbname = databaseGwejh user = postgres password = productive hostaddr = 127.0.0.1 port = 5432");
		if(C.is_open()){
			std::cout << "Opened database successfully: " << C.dbname() << std::endl;
		} else {
			std::cout << "Cannot open database" << std::endl;
			return 1;
		}
			/* Create SQL statement */
		sql = "CREATE TABLE COMPANY(" \
		       "ID INT PRIMARY KEY NOT NULL," \
		       "NAME 		TEXT NOT NULL," \
		       "AGE 		INT NOT NULL," \
		       "ADDRESS 	CHAR(50)," \
		       "SALARY 		REAL );";

			/* Create a transactional object. */
		pqxx::work W(C);

		W.exec(sql); // execute SQL statement.
		W.commit(); // commmit the transaction
		
		std::cout << "Table created successfully" << std::endl;

		C.disconnect();
	} catch(const std::exception &e){
		std::cerr << e.what() << std::endl;
		return 1;
	}

	return 0;
}
