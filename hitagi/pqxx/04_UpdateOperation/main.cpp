#include <exception>
#include <iostream>
#include <ostream>
#include <pqxx/pqxx>

int main(){
	char *sql;

	try {
		pqxx::connection C("dbname = databaseGwejh user = postgres password = productive \
		hostaddr = 127.0.0.1 port = 5432");
		if(C.is_open()){
			std::cout << "Opened database successfully: " << C.dbname() << std::endl;
		} else {
			std::cout << "Can't open database" << std::endl;
			return 1;
		}

			/* Create transactional object */
		pqxx::work W(C);
			/* Create sql update statement */
		sql = "UPDATE COMPANY set SALARY = 25000.00 where ID=1";
			/* Execute sql query */
		W.exec(sql);
		W.commit();
		std::cout << "Record update successfully" << std::endl;
			/* Create sql select statement */
		sql = "SELECT * from COMPANY";
			/* Create a non-transactional */
		pqxx::nontransaction N(C);
			/* Execute sql query */
		pqxx::result R(N.exec(sql));

			/* List down all the record */
		for(pqxx::result::const_iterator c = R.begin(); c != R.end(); ++c){
			std::cout << "ID = " << c[0].as<int>() << std::endl;
			std::cout << "Name = " << c[1].as<std::string>() << std::endl;
			std::cout << "Age = " << c[2].as<int>() << std::endl;
			std::cout << "Address = " << c[3].as<std::string>() << std::endl;
			std::cout << "Salary = " << c[4].as<float>() << std::endl;
		}
		std::cout << "Operational done successfully" << std::endl;
		C.disconnect();
	} catch(const std::exception &e){
		std::cerr << e.what() << std::endl;
	}

	return 0;
}
