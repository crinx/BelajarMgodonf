const makanan = new Map([
    [1, "Soto"],
    [2, "Bakso"],
    [3, "Rangginang"]
]);

console.log(makanan);

console.log(makanan.get(2)); // get value, output = Bakso
makanan.set(4, "Roti"); // adding new key-value

console.log(makanan.get(4));

console.log(makanan.has(1));
console.log(makanan.delete(1)); // delete key-value

console.log(makanan);
