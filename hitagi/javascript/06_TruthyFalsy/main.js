/* expression without return boolean value
 * Number 0
 * BigInt 0n
 * Zero string "" or ''
 * null
 * undefined
 * NaN (Not a Number)
 */

let number;

if (number) {
    console.log('Halo, ${name}');
} else {
    console.log("Not a Number");
}
