/* abstraction or generic method */
const bisaKirimPesan = self => ({
	kirimPesab: () => console.log('kirim pesan: ', self.pesan)
});

const cekNomor = self => ({
	iniValid: () =>  console.log('nomor valid', self.dari)
});

/* compotition object */
const perpesan = (dari, pesan, toko) => {
	/* attributes */
	const self = {
		dari,
		pesan,
		toko
	};

	/* method */
	perilakuPerpesan = self => ({
		buatKatalog: () => console.log('katalog telah dibuat: ', self.toko)
	});

	/* object compotition */
	return Object.assign(self, perilakuPerpesan(self), bisaKirimPesan(self), cekNomor(self));
};

/* code goes brrr */
const test = perpesan('Harry', 'Selamat Pagi!', 'Toko Beliau');
test.buatKatalog();
test.kirimPesab();
