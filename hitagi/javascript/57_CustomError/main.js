class diluarJangkauanArray extends Error {
    constructor(message){
        super(message);
        this.name = 'index tidak terjangkau array';
    }
}

const siswa = ['Raficky', 'Robert', 'Cristina'];

try {
    console.log(siswa[2]);

    if(!siswa[3]) {
        throw new diluarJangkauanArray("'siswa[3]' undefined");
    }
    
} catch(error) {
   if(error instanceof diluarJangkauanArray){
        console.log(`LUWH NOOBS! LIHAR ERRORNYA: ${error.name}!`);
   } else if(error instanceof ReferenceError){
        console.log(error.massage);
   } else {
        console.log(error.stack);
   }
}
