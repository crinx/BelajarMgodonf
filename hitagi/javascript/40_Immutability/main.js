/* Dont manipulate exiting object, when necessary put a 
 * value from this object and copy to new different object 
 */

const pengguna = {
	namaDepan: "Cristino", // typo
	namaBelakang: "Yue"
}

const buatPenggunaDenganNamaDepanBaru = (namaDepanBaru, pengguna) => {
	return {...pengguna, namaDepan: namaDepanBaru}
}

const penggunaBaru = buatPenggunaDenganNamaDepanBaru('Cristina', pengguna);

console.log(penggunaBaru); // put into new object
console.log(pengguna); // without modify real object
