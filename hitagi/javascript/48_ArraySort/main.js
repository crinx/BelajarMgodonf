/* arr.sort([compareFunction]) */
const bulan = ['Feb', 'Apr', 'Jan', 'Des', 'Nov'];
bulan.sort();
console.log(bulan);

const angka = [1, 2, 8, 9, 3, 6];
angka.sort();
console.log(angka);

const nilai = [34, 76, 89, 100];
const perbandingan = (a, b) => {
	return a - b;
};
/* dikomparasi terlabih dahulu, mana yang negatif mana yang positif. dari situ tahu mana yang besar mana yang kecil */
const mengurutkan = nilai.sort(perbandingan);
console.log(mengurutkan);
