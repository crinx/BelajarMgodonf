function membuatTempe(...bahan){
    mengumpulkanBahan(bahan)
        .then(mencuci)
        .then(merebus)
        .then(meragi)
        .then(mengemasi);
}
