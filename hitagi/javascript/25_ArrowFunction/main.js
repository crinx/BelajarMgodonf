// arrow function

const hello = (konsidi1, konndisi2) => {
    console.log("Hello World!");
}

hello();

// if only have one condition, can remove kurung

const hello1 = kondisi => {
    console.log("Hello World!");
}

hello1();

// if body only have one line, can remove bracket
// can return automacly when only have one line body

const hello2 = kondisi => console.log("Hello World!");

hello2();
