// imperative:
const nama = ['Robert', 'Joe', 'Kevin', 'Cristina'];

for(let i = 0; i < nama.length; i++){
	console.log(`${nama[i]}`);
}

// deklarative:
nama.forEach((someVariable) => {
	console.log(`${someVariable}`);
});

/* cannot use break and continue statement like for loop and while loop */
