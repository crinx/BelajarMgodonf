const name = {
    firstName: "Koyomi",
    lastName: "Araragi",
    age: 19
};

const {firstName, lastName, age} = name; // destructuring statement.

console.log(name.firstName); // original
console.log(firstName); // destructured

// default value when adding new object
const {isMale} = name; // undefined because object not have value
console.log(isMale);

const {isStrong = true} = name; // default value is true
console.log(isStrong);

// assigning to different local variable names
const waifu = {
    nameLengkap: "Senjougahara Hitagi",
    age: null
};

const {nameLengkap: localNamaLengkap} = waifu; // nameLengkap to localNamaLengkap
console.log(localNamaLengkap);
