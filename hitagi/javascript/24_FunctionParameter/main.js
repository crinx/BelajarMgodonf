// function parameter with destructured object

const pengguna = {
    name: "Hitagi",
    age: 19
};

function perkenalan({name, age}){
    console.log(`Perkenalkan, nama saya ${name}, saya berusia ${age} tahun.`);
}

perkenalan(pengguna);

// default parameter value

function penjumlahan(a = 1, b = 1){
    console.log(a + b);
}

penjumlahan();

// rest parameter

function sum(...numbers) {
    let result = 0;
    for (let number of numbers) {
        result += number;
    }
    return result;
}

console.log(sum(1, 2, 3, 4, 5));
