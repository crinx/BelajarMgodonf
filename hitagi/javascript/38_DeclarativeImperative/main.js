/* this code is just example from "how to solve" or imperative */
const nama = ['Albert', 'John', 'Cristina', 'Kevin'];

const namaBaruDenganTandaSeru = [];

for (let i = 0; i < nama.length; i++){
	namaBaruDenganTandaSeru.push(`${nama[i]}!`);
}

console.log(namaBaruDenganTandaSeru);

/* code goes brrr */
const name = ['Alex', 'Robert', 'Lina', 'Felix'];

const namaDenganTandaSeru = name.map((name) => `${name}!`);

console.log(namaDenganTandaSeru);
