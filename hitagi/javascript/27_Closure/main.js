const main = () => {
    let nama = "Mikan"; // local variable

    const bicara = () => { // inner function
        console.log(nama); // lexical scope / can access variable on parrent function
    }

    bicara();
}

main();
