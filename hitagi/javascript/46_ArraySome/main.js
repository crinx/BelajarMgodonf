/* arr.some(callback(element, [index], [array]), [thisArg]) */
const angka = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0];

const genap = angka.some(someVariable => someVariable % 2 === 0);
/* checking angka array's is genap or not, so array some return a boolean value */

console.log(genap);
