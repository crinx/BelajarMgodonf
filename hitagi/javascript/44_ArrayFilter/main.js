const nilaiSiswa = [
	{
		nama: 'Robert',
		nilai: 90,
	},
	{
		nama: 'William',
		nilai: 34,
	},
	{
		nama: 'Cristina',
		nilai: 100,
	},
];

const beasiswa = nilaiSiswa.filter((someVariable) => someVariable.nilai > 35 );
/* same as array map */

console.log(beasiswa);
console.log(...nilaiSiswa);
