class pesan {
	constructor(pengirim){
		this.from = pengirim; // public
		this._kontak = []; // private
	}
	
	mesan(input, kepada){
		console.log(`isi pesan: ${input} kepada ${kepada}, dari ${this.from}`);
		this._kontak.push(kepada);
	}

	listKontak(){
		return this._kontak;
	}
}

const ngirim = new pesan('harry');
ngirim.mesan('Hello World!', 'cristina');
console.log(ngirim.listKontak());
