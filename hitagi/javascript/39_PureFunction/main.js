/* pure function
 * Returns the same value when the inputs (parameter values) are the same.
 * Just depends on the given argument.
 * Does not cause side effects. 
 */

/* not pure function */
let pi = 3.14;

const luasLingkaran = (jariJari) => {
	return pi * (jariJari * jariJari);
}

console.log(luasLingkaran(9) + ' cm');
pi = 4;
console.log(luasLingkaran(9) + ' cm'); // different output with same parameter

/* pure function */
const kelilingLingkaran = (diameter) => {
	return 3.14 * diameter;
}

console.log(kelilingLingkaran(9) + ' cm');
console.log(kelilingLingkaran(9) + ' cm'); // same output with same parameter
