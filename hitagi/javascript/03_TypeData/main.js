let x; // undefined
console.log(typeof(x));

let y = null; // null
console.log(y);

let z = 3; // number
console.log(typeof(z));
console.log(z);

let a = 1827369182328363912837219827316928351n; // bigInt / bignumber.
console.log(a);

let b = "Senjougahara Hitagi"; // string
console.log(b);

let c = a === b; // boolean (true or false)
console.log(c);

let id1 = Symbol(3128); // symbol
let id2 = Symbol(3128);
console.log(id1 == id4);
