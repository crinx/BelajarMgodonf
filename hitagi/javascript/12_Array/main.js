let waifu = ["Kurumi Tokisaki", 19, 160, "Twintail"];
console.log(`Perkenalkan waifu saya adalah ` + waifu[0]);
console.log(`Dia berusia ` + waifu[1]);
console.log(`Tingginya sekitar ` + waifu[2]);
console.log(`Gaya rambut kesukaannya adalah ` + waifu[3]);

const waifuGwejh = ["Senjougahara Hitagi", 19, "Monogatari"];
waifuGwejh.push("Araragi"); // menambahkan data array
console.log(waifuGwejh);

waifuGwejh.pop(); // menghapus data array paling belakang
console.log(waifuGwejh);

waifuGwejh.shift(); // menghapus data array paling depan
console.log(waifuGwejh);

waifuGwejh.unshift("Senjougahara");
console.log(waifuGwejh); // menambahkan dara array paling depan

delete waifuGwejh[2];
console.log(waifuGwejh); // menghapus data array sesuai urutan index namun posisi tetap kosong dan tidak ikut terhapus

waifuGwejh.splice(2, 1); // menghapus index dan posisinya
console.log(waifuGwejh);
