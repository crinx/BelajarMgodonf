class parrentClass {
	constructor(input) {
		this.from = input;
		this._who = [];
	}

	someFunction(input, who){
		console.log(`this input is ${input} and other input is ${who} from ${this.from}`);
		this._who.push(who);
	}

	anyOne(){
		return this._who;
	}
}

class childClass extends parrentClass {
	constructor(input){
		super(input);
		this.people = 'police';
		this.dangerous = true;
	}

	peopleProfile(){
		return `people is ${this.people}, is ${this.dangerous ? 'Dangerous' : 'Personal'}`;
	}
}

// program implementation

const terorism = new childClass('Robert');
const tese = new parrentClass('Lucas');
console.log(terorism.peopleProfile());
tese.someFunction('Hello Terorism!', `Cristina`);
console.log(tese.anyOne());
