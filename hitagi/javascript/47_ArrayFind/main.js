/* arr.find(callback(element, [index], [array]), [thisArg]); */
const nilaiSiswa = [
	{
		nama: 'Robert',
		nilai: 78,
	},
	{
		nama: 'Joe',
		nilai: 87,
	},
	{
		nama: 'Cristina',
		nilai: 100,
	},
];

const namaSiswa = nilaiSiswa.find((someVariable) => someVariable.nama === 'Cristina');
/* return a value instead of boolean value != array some */

console.log(namaSiswa);
