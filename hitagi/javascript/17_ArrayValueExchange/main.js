let a = 2, b = 14; // default value
console.log("a = " + a);
console.log("b = " + b);

[a, b] = [b, a] // exchange value array

console.log("a = " + a);
console.log("b = " + b);
