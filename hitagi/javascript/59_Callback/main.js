const pesanNasi = callback => {
    let nasi = null;
    console.log("Sedang menanak nasi, mohon tunggu...");
    setTimeout(() => {
        nasi = "Nasi sudah  jadi!"
        callback(nasi);
    }, 4000);
}

pesanNasi(nasi => {
    console.log(nasi);
});
