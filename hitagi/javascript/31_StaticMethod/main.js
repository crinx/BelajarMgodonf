/* use function in the class without instantiated first. */
class pesan {
    static isValidEmail(email){
        return typeof email === 'string';
    }
}

/* then, use it */
console.log(pesan.isValidEmail('alifnurcahyo'));
