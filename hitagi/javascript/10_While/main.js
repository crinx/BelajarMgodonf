let x = 1;

// while loop statement
while (x < 10) {
    console.log(x);
    x++;
}

// do-while loop statement
do {
    console.log(x);
    ++x
} while (x < 10);
