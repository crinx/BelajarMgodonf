const waifu = ["hitagi", "kurumi", "futaba"]; // default array

const [, , thirdWaifu] = waifu; // destructuring array

console.log(thirdWaifu);

const makanan = ["nasi", "daging", "sup", "ikan"] // default array

let dia = "kopi"; // adding new object

[dia] = makanan; // destructuring asignment, adding dia to makanan array

console.log(dia);

const minuman = ["es buah"];
const [esBuah, esRoti] = minuman;

console.log(esRoti); // undefinded, because array without value

const [, , esTeh =  "es teh"] = minuman;

console.log(esTeh); // esTeh not undefined because have value


