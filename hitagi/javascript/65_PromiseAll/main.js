let sumberDaya = {
    stok: {
        beras: 1000,
        air: 500,
    },
    kukerSibuk: false,
}

const cekKeberadaan = () => {
    return new Promise((bisa, tidak) => {
        setTimeout(() => {
            if(!sumberDaya.kukerSibuk){
                bisa('Mesin kuker shapp digunakan');
            } else{
                tidak('Mesin kuker sangad sibuk');
            }
        }, 3000);
    });
};

const cekStok = () => {
    return new Promise((bisa, tidak) => {
        sumberDaya.kukerSibuk = true;
        setTimeout(() => {
            if(sumberDaya.stok.beras >= 150 && sumberDaya.stok.air >= 50){
                bisa('stok cukup, bisa menanak nasi');
            } else {
                tidak('stok tidak cukup');
            }
        }, 2000);
    });
};

const mencuciBeras = () => {
    console.log('Beras sedang dicuci...');
    return new Promise((bisa, tidak) => {
        setTimeout(() => {
            bisa('beras sudah dicuci');
        }, 2000);
    });
};

const cuciPanciKuker = () => {
    console.log('Panci kuker sedang dicuci...');
    return new Promise((bisa, tidak) => {
       setTimeout(() => {
            bisa('panci kuker sudah bersih');
       }, 1000);
    });
};

const nanakBeras = () => {
    console.log("Sedang menanak nasi anda...");
    return new Promise((bisa, tidak) => {
        setTimeout(() => {
            bisa('nasi sudah matang');
        }, 10000)
    });
};

function menanakNasi(){
    cekKeberadaan()
        .then((isi) => {
            console.log(isi);
            return cekStok();
        })
        .then((isi) => {
            console.log(isi);
            const satset = [cuciPanciKuker(), mencuciBeras()]; // based on array
            return Promise.all(satset); // return all promise
        })
        .then((isi) => {
            console.log(isi);
            return nanakBeras();
        })
        .then((isi) => {
            console.log(isi);
            sumberDaya.kukerSibuk = false;
        })
        .catch(tidakBisa => {
            console.log(tidakBisa);
            sumberDaya.kukerSibuk = false;
        });
}

menanakNasi();
