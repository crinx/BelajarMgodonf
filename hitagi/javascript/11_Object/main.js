const waifu = {
    name: "Senjougahara Hitagi", // name is key
    age: 19, // 19 is value
    "eyes color": "purple" // if choosing key name with spacing, use tanda petik instead
};

console.log(`Hai! Perkenalkan nama waifu saya adalaj ${waifu.name}`);
console.log(`Dia Berusia ${waifu.age} tahun`);
console.log(`Memiliki warna rambut ${waifu["eyes color"]}`);

// modification value
waifu.name = "Kurumi Tokisaki";
waifu["eyes color"] = "red";

console.log(waifu);

waifu.height = 160; // adding new property(key with value)
delete waifu.height; // deleting property
