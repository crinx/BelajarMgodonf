let language = "Indonesian";
let greeting = null;

switch (language) {
    case "English" : // condition statement
        greeting = "Good Night!";
        break; // exit statement
    case "Indonesian" :
        greeting = "Selamat Malam!";
        break;
    default : // else condition
        greeting = "Konbanwa!";
}

console.log(greeting);
