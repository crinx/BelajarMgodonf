const stok = {
    beras: 1000,
    air: 500
}

const cekStok = () => {
    return new Promise((cukub, tidakCukub) => {
        if(stok.beras >= 150 && stok.air >= 50){
            cukub('Stok masih cukup, bisa menanak nasi');
        } else {
            tidakCukub('Stok tidak mencukupi');
        }
    });
};

const isCukup = iya => {
    console.log(iya);
}

const isTidakCukup = iya => {
    console.log(iya);
}

cekStok().then(isCukup, isTidakCukup);
