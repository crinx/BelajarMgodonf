class pesan {
    pengirim(){
        this.dari = '';
    }
    kirimPesan(pesan, ke, dari){ // class method
        console.log(`kamu mengirim: ${pesan} ke ${ke} dari ${this.dari}`);
    }
}

const mesan = new pesan(); // instantiation first before use kirimPesan function
mesan.dari = 'alifnurcahyo101101@gmail.com';
mesan.kirimPesan('Hemlo Womrld', 'senjougaharaHitagi@gmail.com');
