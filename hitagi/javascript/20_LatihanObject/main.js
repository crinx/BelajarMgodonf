restaurant = {
	name: "Mister Donut",
    city: "Ponorogo",
    "favorite drink": "Coffe",
    "favorite food": "Honey Dip",
    "isVegan": true
};

let name = restaurant.name;
console.log(name);

let favoriteDrink = restaurant["favorite drink"];
console.log(favoriteDrink);
