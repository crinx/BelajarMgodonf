const name = {
    firstName: "Koyomi",
    lastName: "Araragi",
    age: 19
};

let firstName = "Hitagi";
let lastName = "Senjougahara";

({firstName, lastName} = name); // inisialization new value via destructuring object

console.log(firstName);
console.log(lastName);
