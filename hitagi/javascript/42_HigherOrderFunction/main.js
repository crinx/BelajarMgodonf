const kumpulanNama = ['Robert', 'Alex', 'Cristina', 'Joe'];

const petaArray = (arr, aksi) => {
	const perulangan = (arr, aksi, arrayBaru = [], index = 0) => {
		const objek = arr[index];
		if(!objek) return arrayBaru;
		return perulangan(arr, aksi, [...arrayBaru, aksi(arr[index])], index + 1);
	}
	return perulangan(arr, aksi);
}

const namaBaru = petaArray(kumpulanNama, (kumpulanNama) => `${kumpulanNama}Tzy`);

console.log({
	kumpulanNama,
	namaBaru
});
