const nilaiSiswa = [
	{
		nama: 'Robert',
		nilai: 90,
	},
	{
		nama: 'William',
		nilai: 34,
	},
	{
		nama: 'Cristina',
		nilai: 100,
	},
];

/* arr.reduce(callback(accumulator, currentValue, [currentIndex], [array]), [initialValue]) */
const totalNilai = nilaiSiswa.reduce((someVariable, someVariableToo) => someVariable + someVariableToo.nilai, 0 );

console.log(totalNilai);
