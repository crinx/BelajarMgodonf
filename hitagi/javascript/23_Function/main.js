// regular function
function hai(bahasa){
    if (bahasa == "indonesia"){
        return "HAI!";
    } else if (bahasa == "inggris"){
        return "HII!";
    }
}

console.log(hai("inggris"));

// expression function (anonymus function)
const anotherHai = function(bahasa){
    if (bahasa == "indonesia"){
        return "HAI!";
    } else if (bahasa == "inggris"){
        return "HII!";
    }
}

console.log(anotherHai("indonesia"));
