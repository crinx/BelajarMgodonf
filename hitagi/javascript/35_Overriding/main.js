class pesan {
	constructor(input){
		this.from = input;
		this._kontak = [];
	}

	mesan(pesan, kepada){
		console.log(`isi pesan: ${pesan} kepada ${kepada}, dari ${this.from}`);
		this._kontak.push(kepada);
	}
}

class perpesan extends pesan {
	constructor(panggilan, isDangerous){
		super(panggilan); // overriding to constructor
		this.pengguna = panggilan;
		this.dangerous = isDangerous;
	}

	mesan(pesan, kepada){
		super.mesan(pesan, kepada); // overriding to method / function
		console.log('dikirimkan melalui perpesan');
	}
}

// code goess brrrr
const perpesanan = new perpesan('robert', true);
console.log(perpesanan.from);

perpesanan.mesan('Hello World!', 'Harry');
