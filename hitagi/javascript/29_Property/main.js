/*
 * javascript have two access identifier for a field
 * public and private
 */

class pesan {
    pengirim(){
        this.from = '';
    }
    kirimPesan(pesan, ke, from){
        console.log(`kamu mengirim: ${pesan}, ke ${ke} dari ${this.from}`)
    }
}

const pesan1 = new pesan();
pesan1.from = 'alifnurcahyo101101@gmail.com';
pesan1.kirimPesan('Hallow', 'senjougaharaHitagi@gmail.com');
pesan1.kontak = ['senjougaharaHitagi@gmail.com'];

/*
 * if we want to change atribute kontak on pesan class to private,
 * javascript have three method
 */

// first methode, using var. Can use on class writed in function statement
function perpesan(){
    this.from = '';
    var kontak = [];
}

// second methode, can ise on class writed use class and function statement
class berpesan {
    pengirim(){
        this._kontak = [];
        this.from = '';
    }
}

// third methode, adding prefix #. Can use only when class writed in class stetement
class mesan {
    #kontak = [];
    pengirim(){
        this.from = '';
    }
}

// if access variable kontak directly, for this return is undefined. Because this is private 
const test = new mesan();
console.log(test.kontak); // undefined
