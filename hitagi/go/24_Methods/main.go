package main

import "fmt"

type siswa struct {
	nama string
	nis  int
}

func (x *siswa) hajimemaste(val int) {
	fmt.Println(x.nama)
	fmt.Println(x.nis + val)
}

func main() {
	input := 3
	kelasX := &siswa{"Cristina", 123}
	kelasX.hajimemaste(input)
}
