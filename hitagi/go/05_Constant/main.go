package main

import "fmt"

func main() {
	const phi float32 = 3.14 // nilai tetap yang harus dideklarasikan dan tidak dapat diubah
	fmt.Println(phi)
}
