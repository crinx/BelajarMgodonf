package main

import "fmt"

func main() {
	x := 4

	switch {
		case x<6 && x>0:
			fmt.Println("something")
		default:
			fmt.Println("something else")
	}
}
