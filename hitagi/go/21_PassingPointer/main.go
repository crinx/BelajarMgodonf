package main

import "fmt"

func temp(val int){ // change value
	val = 8
}

func change(ptr *int){ // pointer as input
	*ptr = 8 // change value using pointer
}

func main(){
	x := 9

	temp(x)
	fmt.Println(x)

	change(&x) // adrress as value
	fmt.Println(x)
}
