package main

import "fmt"

/* maps are called dictionaries, associative array, or hash table */

func main(){
	m := make(map[string]int) // declare map
	m["Cristina"] = 21

	o := map[string]int{
		"Robert": 22,
		"Dio": 19,
	}

	fmt.Println(m)
	fmt.Println(m["Cristina"])

	fmt.Println(o)
	fmt.Println(o["Robert"])
	delete(o, "Dio")
	fmt.Println(o)
}
