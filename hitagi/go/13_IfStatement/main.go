package main

import "fmt"

func main() {
	// didalam bahasa go bisa mendeklarasikan variabel di if statement
	if a := 3; a >= 1 {
		fmt.Println("a lebih besar dari 1")
	} else {
		fmt.Println("a lebih kecil dari 1")
	}
}
