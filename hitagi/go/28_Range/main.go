package main

import "fmt"

func main(){
	a := make([]int, 5)
	a[1] = 2
	a[2] = 5

	for i, v := range a {
		fmt.Println(i, v)
	}

	for _, v := range a { // skip the index using _
		fmt.Println(v)
	}
	
	for i := range a {
		fmt.Println(i)
	}

	res := 0

nums := [3]int{2, 4, 6}

for i, v := range nums {

  if i%2==0 {

    res += v

  }

}

fmt.Println(res)
}
