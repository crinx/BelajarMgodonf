package main

import "fmt"

func main() {
	a := 3
	b := 4

	fmt.Println( a == b )
	fmt.Println( a > b )
	fmt.Println( a != b )
}
