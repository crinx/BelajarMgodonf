package main

import "fmt"

func hello(){
	fmt.Println("Hello")
}

func loop(){
	fmt.Println("Mulai For Loop...")
	for i := 0; i < 5; i++ {
		defer fmt.Println(i)
	}
	fmt.Println("For Loop Selesai")
}

func main(){
	defer hello() // waiting main function done before calling defer function
	fmt.Println("Alif")

	defer loop() // defer is stack
}
