package main

import "fmt"

func main(){
	var p *int // declare pointer
	x := 9
	p = &x // assign p variable to x address

	fmt.Println(p) // memory address
	fmt.Println(*p) // value of memory

	*p = 12 // dereferencing
	fmt.Println(*p)
}
