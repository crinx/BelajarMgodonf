package main
import "fmt"

func sum(val ...string){
    total := 0
    for _, v := range val {
        if v == "w" {
            total += 3
        } else if v == "d" {
            total += 1
        } else {
	    total += 0
	}
        fmt.Println(total)
    }
}

func main() {
    var a string
    results := []string{"w", "l", "w", "d", "w", "l", "l", "l", "d", "d", "w", "l", "w", "d"}

    fmt.Scanln(&a)
    results = append(results, a)

    sum(results...)
}
