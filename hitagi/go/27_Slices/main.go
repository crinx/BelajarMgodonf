package main

import "fmt"

func main(){
	a := [5]int{1, 3, 8, 2, 9}
	var s []int = a[1:3] // 1 is index, 3 is length of array
	b := make([]int, 5) // create a slice
	
	fmt.Println(a)
	fmt.Println(s)
	fmt.Println(s[0]) // get value like array

	s[0] = 8 // not change array value, it just decribes a section of an underlaying array
	fmt.Println(s[0])
	fmt.Println(s)
	fmt.Println(a)

	fmt.Println(b)
	b = append(b, 4) // adding new elements
	b[1] = 9
	fmt.Println(b)
}
