package main

import (
	"fmt"
	"time"
)

func hackingNasa(from, to int, ch chan bool){
	for i := from; i <= to; i += 10 {
		time.Sleep(500 * time.Millisecond)
		fmt.Println("Hacking Nasa...", i, "%")
	}
	fmt.Println("Nasa Hacked")
	ch <- true
}

func penjumlahan(a, b int, cj chan int){
	hasil := a+b
	cj <- hasil
}


func main(){
	ch := make(chan bool) // declare channel
	qch := make(chan int)
	go hackingNasa(0, 100, ch) // executed concurrently
	go penjumlahan(19, 123, qch)
	fmt.Println(<-qch)
	<- ch // to wait gorountines function
}
