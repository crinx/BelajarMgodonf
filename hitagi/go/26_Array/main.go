package main

import "fmt"

func main(){
	var a [5]int // declare array
	a = [5]int{1, 2, 3, 4, 5} // adding value to array

	fmt.Println(a)
}
