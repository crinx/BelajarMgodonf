package main

import "fmt"

func main() {
	// digunakan ketika berada pada lebih dari satu kondisi
	a := 9
	b := 1

	// AND
	fmt.Println( a != b && a >= b )
	// OR
	fmt.Println( a == b || a >= b )
	// NOT
	fmt.Println( !( a < b ))
}
