package main

import "fmt"

func print() {
	fmt.Println("Hello World!")
}

func main() {
	print()
}

// komentar satu baris dengan double slash
/*
	komentar beberapa baris seperti ini
*/
