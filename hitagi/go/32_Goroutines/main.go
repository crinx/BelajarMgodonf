package main

import (
	"fmt"
	"time"
)

func hackingNasa(from, to int){
	for i := from; i <= to; i += 10 {
		time.Sleep(500 * time.Millisecond)
		fmt.Println("Hacking Nasa...", i, "%")
	}
	fmt.Println("Nasa Hacked")
}

func superHackingNasa(from, to int){
	for i := from; i <= to; i += 10 {
		time.Sleep(50 * time.Millisecond)
		fmt.Println("Hacking Nasa...", i, "%")
	}
	fmt.Println("Nasa Hacked")
}

func main(){ // not wait function when function are executed concurrently
	go hackingNasa(0, 100) // executed concurrently
	go superHackingNasa(0, 100)
//	time.Sleep(5000 * time.Millisecond) // to wait gorountines function
}
