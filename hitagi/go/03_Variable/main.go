package main

import "fmt"

func main() {
	var i int // mendefinisikan variable terlebih dahulu dengan `var nameOfVariable typeData`
	i = 6 // mengisi nilai variable
	fmt.Println(i)

	var j, k int = 3, i
	fmt.Println(j)
	fmt.Println(k)

	a, b, c := 1, 4, 7
	fmt.Println(a)
	fmt.Println(b)
	fmt.Println(c)
}
