package main

import "fmt"

func main() {
	y := 6
	x := 9

	// penjumlahan
	penjumlahan := y + x
	fmt.Println(penjumlahan)

	// pengurangan
	pengurangan := y - x
	fmt.Println(pengurangan)

	// perkalian
	perkalian := y * x
	fmt.Println(perkalian)

	// pembagian
	pembagian := x / y
	fmt.Println(pembagian)

	// modulus
	modulus := x % y
	fmt.Println(modulus)
}
