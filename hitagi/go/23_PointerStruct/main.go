package main

import "fmt"

type siswa struct {
	nama string
	nis  int
}

func main() {
	kelasXI := siswa{"Cristina", 312} // declare struct
	p := &kelasXI                     // parser pointer to struct address

	fmt.Println(p.nama)
	fmt.Println(p.nis)

	kelasX := &siswa{"Robert", 341} // declare struct using pointer

	fmt.Println(kelasX.nama)
	fmt.Println(kelasX.nis)
}
