package main

import "fmt"

func penjumlahan(nums ...int){ // ... is variadic declaration
	total := 0
	for _, v := range nums{
		total += v
		fmt.Println(v)
	}
	fmt.Println("---- +")
	fmt.Println(total)
	fmt.Println()
}

func main(){
	penjumlahan(12, 13, 14)	
	s := []int{2,3,8}
	penjumlahan(s...) // input as array
}
