package main

import "fmt"

func main() {
	/* float32 - a single-precision floating point value.
	 * float64 - a double-precision floating point value.
	 * string - a text value.
	 * bool - Boolean true or false. 
	 */

	//	 The difference between float32 and float64 is the precision, meaning that float64 will represent the number with higher accuracy, but will take more space in memory.

	var a int = 3
	var b float32 = 4.9
	var c string = "Kurumi"
	var d bool = false

	fmt.Println(a)
	fmt.Println(b)
	fmt.Println(c)
	fmt.Println(d)
}
