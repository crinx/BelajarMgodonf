package main

import "fmt"

type Timer struct{
    id string
    value int
}

func(t *Timer)tick(val int){
    fmt.Println(val + 1)
}

func main() {
    var x int
    fmt.Scanln(&x)

    t := Timer{"timer1", 0}
    
    for i:=0;i<x;i++ {
        t.tick(i)
    }
}
