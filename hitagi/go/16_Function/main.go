package main

import "fmt"

func main(){
	helloWorld()
	helloUser("Alif")
	fmt.Println(penjumlahan(15, 87))
	fmt.Println(pertukaran(4, 7))
}

func helloWorld(){ // without argument
	fmt.Println("Hello World")
}

func helloUser(name string){ // with single argument
	fmt.Println("Hello " + name)
}

func penjumlahan(a, b int) int{ // with single return value
	return a+b
}

func pertukaran(a, b int)(int, int){ // with multiple return value
	return b, a
}
