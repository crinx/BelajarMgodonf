package main

import "fmt"

const y = 8 // global variable

func someFunc()int{
	var x = 9 // local variable
	return x
}

func main(){
	fmt.Println(y)
	fmt.Println(someFunc())
}
