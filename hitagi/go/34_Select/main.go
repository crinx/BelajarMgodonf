package main

import (
	"fmt"
	"time"
)

func a(ch chan int){
	time.Sleep(10 * time.Millisecond)
	ch <- 2
}

func b(ch chan int){
	time.Sleep(20 * time.Millisecond)
	ch <- 4
}

func main(){
	qch := make(chan int)
	wch := make(chan int)

	go a(qch)
	go b(wch)

	for {
		select {
			case x := <- qch:
				fmt.Println(x)
				return
			case y := <- wch:
				fmt.Println(y)
				return
			default: 
				fmt.Println("Nothing")
				time.Sleep(10 * time.Millisecond)
		}
	}	
}
