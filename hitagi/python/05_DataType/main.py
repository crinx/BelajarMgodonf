angka = 15
angkaDesimal = 3.14
nama = "hitagi"
benar = True
gudang = [1, 's', True, 1.42, "Kurumi >///<"] # ordered sequence.
paket = (1, 's', True, 1.42, "Kurumi >///<") # immutable.
angka = {1, 2, 4, 6, 9, 0, 2, 1} # unordered sequence.
data = {'nama': 'hitagi', 'umur': 18, 'domisili': 'jepang', 'perawan': True} # key value.

# print data type and memory address.
print(type(angka), id(angka))
print(type(angkaDesimal), id(angkaDesimal))
print(type(nama), id(nama))
print(type(benar), id(benar))
print(type(gudang), id(gudang))
print(type(paket), id(paket))
print(type(angka), id(angka))
print(type(data), id(data))
