deretAngka = [1, 2, 3, 4, 6, 7, 4, 5, 9, 0]

# putting value from list to variable.
for i in deretAngka:
    print(i)

# range(start, stop, step).
for i in range(0, 10, 2):
    print(i)

# nested for loop.
for i in range(1, 3):
    for j in range(1, 3):
        print(i, j)

# break statement.
for i in range(2):
    print(i)
    for j in range(10):
        print(j)
        if j > 5:
            break

# continue statement.
for i in 'Hitagi Senjougahara':
    if i == ' ':
        continue
    print(i)

# else after for loop.
deret = [1, 2, 4, 3, 5]

for i in deret:
    if i == 4:
        print("angka ditemukan")
        break
else:
    print("angka tidak ditemukan")
