nama = "hitagi senjougahara"

# get string value with indexing and slicing.
print(nama[0])
print(nama[7:])

# formatted string.
print(f"my name is {nama}") # f indicates the variable is of type string.
print("my name is %s" % (nama))
print("my name is {}".format(nama)) # combining variables with strings and {} as variable placement.

# upper and lower case.
print(nama.upper())
print(nama.lower())

# clean up white space.
print("Kurumi    ".rstrip()) # clear whitespace at the end of the sentence.
print("    Kurumi".lstrip()) # clear whitespace at the beginning of the sentence.
print("    Kurumi    ".strip()) # clear both beginning and end of the sentence.

# remove unused characters.
waifu = "CocoLateCocoCoco"
print(waifu.strip("Coco")) # case sensitive.

# finds a word at the beginning and end of the string.
print(nama.startswith('hitagi'))
print(nama.startswith('senjougahara'))

print(nama.endswith('hitagi'))
print(nama.endswith('senjougahara'))

# splitting and joining strings.
print('-'.join(['Hitagi', 'Senjougahara', '!']))
print('Hitagi-Senjougahara'.split('-'))
print('''Hai,
aku ikan,
aku suka makan ikan.
dan tinggal dirumah.'''.split('\n'))

# replace element strings.
waif = "Tilty best waifu in the world!"
print(waif.replace("Tilty", "Hitagi"))

# checking strings (case sensitive).
waif = "HITAGI123"
print(waif.isupper())
print(waif.islower())
print(waif.isalpha()) # alphabet only.
print(waif.isalnum()) # alphabet and number.
print('123'.isdecimal()) # number only.
print('  '.isspace()) # space only.
print('Hitagi Senjougahara'.istitle()) # capital every first word.

# formatting string.
waifu = "HITAGI"
print(waifu.zfill(10)) # will add 0 at the beginning of the string.
print(waifu.rjust(10, '*')) # will add space at the beginning of the string.
print(waifu.ljust(10, '*')) # will add space at the end of the string.
print(waifu.center(10, '*')) # will add space at the both side of the string.

# string literals.
waifu = "\t\"Hitagi\"\n" # escape character.
print(waifu)

# raw strings.
print(r"\t\"Hitagi\"\n") # prints a string according to whatever input or text is given.

# calculate the length of the element.
print(nama) # space will included.
print(len(nama))

# in and not in
waifuGwejh = "Kurumi kawaii beud coeg"
print("Kurumi" in waifuGwejh) # case sensitive.
print("kawaii" not in waifuGwejh)
