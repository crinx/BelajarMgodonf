# stop when i is equal to 0.
i = 6
while i > 0:
    print(i)
    i = i - 1

# else after while loop.
count = 0
while count < 3:
    print("Kurumi waifu gwejh!")
    count += 1
    if count == 3:
        break
else:
    print("Jangan lupa nonton anime!")
