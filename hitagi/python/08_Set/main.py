angka = {1, 2, 4, 6, 9, 0, 2, 1}
angka2 = {9, 8, 7, 6, 5, 4, 3, 2, 1, 0}

# remove duplication.
print(angka)

union = angka.union(angka2)
print(union)

intersection = angka.intersection(angka2)
print(intersection)

# calculate the length of the element.
lemari = set([1, 2, 3, 4, 5, 6, 7, 7, 1, 5, 2]) # convert list to set, because to clean up duplication.
print(lemari)
print(len(lemari))
