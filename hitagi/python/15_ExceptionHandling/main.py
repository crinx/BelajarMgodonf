# data with string as value.
var_data = {"data_rata_rata": "2.0"}

try: # try to execute this block.
    print(f"rata-rata adalah {var_data['data_rata_rata']/2}.")
except KeyError: # if key not avaible.
    print("data rata-rata tidak ada.")
except TypeError: # if value divaded by integer.
    print("anda tidak bisa membagi bilai dengan tipe data string.")
else: # if the exception does not occur.
    print("program tidak terjadi exception.")
finally: # always executed.
    print("program selesai.")

# raise exception.
angka = -1

if angka < 0:
    raise Exception("angka tidak boleh negatif!")
else:
    for i in range(angka):
        print(i+1)
