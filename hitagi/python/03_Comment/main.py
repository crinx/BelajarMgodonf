# Inline comment.
# comment before or in line with the code, to explain the code specifically.

# variable to store the letter 'a'.
letterA = 'a'

# Block comment.
# a comment block, used to explain complex code.
# can use double quotes or single quotes.

"""
This is a function,
to print the contents of the letterA variable
"""

print(letterA)
