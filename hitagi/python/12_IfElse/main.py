bahan = "animeh"
kawaii = 100

if bahan == "animeh":
    print("saya suka animeh") # if true.
elif bahan == "bahan":
    print("saya suka bahan")
else:
    print("saya tidak suka animeh") # if false.

# one liner version.
if kawaii == 100: print("waifu gwejh kawaii {}%".format(kawaii))

# ternary operator.
waifuGwejh = True
print("kurumi waifu gwejh") if waifuGwejh else print("kurumi bukan waifu gwejh")

betulkah = ("kurumi bukan waifu gwejh", "kurumi waifu gwejh")[waifuGwejh]
print(betulkah)

# pass statement.
if kawaii == 100: 
    pass
else:
    print("kurumi bukan waifu gwejh")
