# from int to float.
angka = 1
angkaDesimal = float(angka)

print(angka)
print(angkaDesimal)

# from float to int.
angkaDesimal = 1.5
angka = int(angkaDesimal)

print(angkaDesimal)
print(angka)

# from int and float to str.
angka = 1
angkaDesimal = 1.5

print(str(angka) + " " + str(angkaDesimal))

# data set conversion.
print(set([1, 2, 3]))
print(tuple({5, 6, 7}))
print(list("hitagi >///<"))

# conversion to dictionary.
print(dict([[1, 2], [3, 4]]))
print(dict([(1, 2.12), (3, 4.44)]))
