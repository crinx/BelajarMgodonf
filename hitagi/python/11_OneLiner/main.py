a = 10
b = 11

print("a: ", a)
print("b: ", b)

# one liner.
# swap the value of variable a to variable b.
a, b = b, a

print("a: ", a)
print("b: ", b)
