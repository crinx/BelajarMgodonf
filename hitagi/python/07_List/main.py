# ordered sequence and mutable.
gudang = [1, 's', True, 1.42, "Kurumi >///<"]

print(gudang[4])

# change value.
gudang[4] = "Hitagi >///<"

print(gudang[4])

# get value from the beginning.
print(gudang[0])

# get value from the end.
print(gudang[-1])

# slicing.
# sequence[start:end:step]
print(gudang[1:4:2])
print(gudang[1:4])
print(gudang[1:])
print(gudang[:4])

# calculate the length of the element.
lemari = [1, 2, 3, 4, 5, 6, 7]
print(lemari)
print(len(lemari))

# get maximum and minimum value from list.
deret = [321, 23, 41, 98, 230]
print(max(deret))
print(min(deret))

# counts how many times the object appears.
deret = [1, 5, 2, 8, 2, 4, 5, 2]
print(deret.count(2))
print(deret.count(5))

nama = "Hitagi Senjougahara"
print(nama.count("a"))

# assign value to multiple variables.
hitagi = ['harum', 'hitam', 'C']
bau = hitagi[0]
kulit = hitagi[1]
cup = hitagi[2]

print(hitagi)
print("hitagi = ", bau, kulit, cup)

# shorting.
waifuGwejh = ["kurumi", "hitagi", "itu ajah"]
waifuGwejh.sort() # case sensitive.
print(waifuGwejh)

waifuGwejh.sort(reverse=True)
print(waifuGwejh)

# list comprehension.
deret = [1, 2, 3, 4, 5]
pangkat = [i**2 for i in deret]
print(deret)
print(pangkat)
