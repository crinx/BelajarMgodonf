# loosely typed.
name = "Kurumi Tokisaki"
age = 18;
salary = 90000.09

print(type(name))
print(type(age))
print(type(salary))

# dynamic typing.
salary = 1900

print(type(salary))
