data = {'nama': 'hitagi', 'umur': 18, 'domisili': 'jepang', 'perawan': True}

# to get a value, you have to use a key, not an index.
print(f"nama: {data['nama']}")
print(f"umur: {data['umur']}")
print(f"domisili: {data['domisili']}")
print(f"perawan: {data['perawan']}")

# update value.
data['umur'] = 19
print(f"umur: {data['umur']}")

# add data.
data['kekasih'] = "Aliffff"
print(f"kekasih: {data['kekasih']}")
print(data)

# remove data.
del data['kekasih']
print(data)
