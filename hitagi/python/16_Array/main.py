import array

# i means integer.
deret = array.array('i', [1, 2, 3, 4, 5])

print(deret)
print(type(deret))

# default value.
# 0 means default value.
# 5 means length of array.
angka = [0 for i in range(5)]

print(angka)

# assign value with for loop.
for i in range(5):
    angka[i] = i

print(angka)

# sequential processing.
kumpulan = [i for i in range(0, 10, 2)]

for i in range(len(kumpulan)):
    # indexing method to get value.
    elemen_sekarang = kumpulan[i]
    # successor index.
    index_selanjutnya = i+1

    if index_selanjutnya < len(kumpulan):
        # to get next value.
        elemen_selanjutnya = kumpulan[index_selanjutnya]
    else:
        elemen_selanjutnya = None
    print(f'{elemen_sekarang} - {elemen_selanjutnya}')

# two pointer algorithm to find largest value.
petualang = [1, 4, 87, 28, 99, 0]
terkuat = petualang[0]

for i in range(1, len(petualang)):
    terlemah = petualang[i]

    if terlemah > terkuat:
        terkuat = terlemah

print(terkuat)
