#!/usr/bin/env bash

# bash does not segregate its variables by "type".

a=3121        # integer
let "a += 1"
echo "a = $a"
echo

b=ikan33          # string
echo "b = $b"
declare -i b      # decaring it an integer doesn't help.
echo "b = $b"

let "b += 1"      # sets the "integer value" of a string to 0.
echo $b

c=BB34            # string
echo "c = $c"
d=${c/BB/23}      # subtitue "23" for "BB"
echo "d = $d"     # integer
let "d += 1"
echo "d = $d"

# null variable
e=''
echo "e = $e"
let "e += 1"    # transformed from null to integer
echo "e = $e"

# undeclarated variable
echo "f = $f"
let "f += 1"
echo "f = $f"   # transformed from undeclarated to integer
