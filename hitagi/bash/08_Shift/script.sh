#!/usr/bin/env bash

# indicating how many positions to shift.
# whill skip $1, $2, $3 and shift $4 and $5 . . .
shift 3

until [ -z "$1" ] ; do
  echo -n "$1"
  shift
done

# nothing echoes!
# when $2 shifts into $1 (and there is no $3 to shift into $2).
# so, it is not a parameter "copy", but a "move".
echo "$2"

exit
