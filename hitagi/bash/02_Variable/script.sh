#!/usr/bin/env bash

# accessing an environment variable.
echo $HOME

# initialized variable.
# no space permitted on either of = sign.
a=32767
b=a

# assignment using let.
let a=15-9
echo $a

# with command substitution.
hell=`echo hello!` # using backquotes.
echo $hell

hall=$(echo holla!)
echo $hall # a newer method.

# $a is simplified form of ${a}.
echo $a
echo ${b}

# escaping whitespace.
c=2\ ---\ mix
echo $c

# escaped.
echo '$a'

# quoting.
echo "$b"
echo "${b}"

# uninitialized variable.
unset a b

# perform aritmathmetic to assigning a value.
echo $d
let d+=5
echo $d
let d+=6
echo $d

# the name of a variable is called an lvalue
# a variable's value is an rvalue because appears on the right
# of an assignment statement.

# readonly variable.
name="hitagi"
readonly name
echo $name

name="kurumi"
echo $name
