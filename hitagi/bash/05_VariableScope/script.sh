#!/usr/bin/env bash

# 1. Local variables
# visible only within a code block or function.
siapaAku(){
  who=$(whoami) # local variable
  echo "Aku adalah $who"
}

siapaAku

# 2. Environmental variabes
# affect the behavior of the shell and user interface.
# $ MOZ_WAYLAND_ENABLE=1 firefox
# 
# * In more general context, each process has an environment.
# * A scripts can only exports varibles to child process,
#   only to command and process which that particular scripts initializes.

# 3. Positional parameters
# I will discuss in the next session.
