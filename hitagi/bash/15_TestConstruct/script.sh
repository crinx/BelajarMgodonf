#!/usr/bin/env bash

# logical AND
((0 && 1))
echo $?

let "num = ((0 && 1))"
echo $num

let "num = ((0 && 1))"
echo $?

# logical OR
((200 || 11))
echo $?

let "num = ((200 || 11))"
echo $num

let "num = ((200 || 11))"
echo $?

# bitwise OR
((200 | 11))
echo $?

let "num = ((200 | 11))"
echo $num

let "num = ((200 | 11))"
echo $?

# exit status of an arithmetic expression is not an error value.
var=-2 && ((var+=2))
echo $?
# will not echo $var!
var=-2 && ((var+=2)) && echo $var
