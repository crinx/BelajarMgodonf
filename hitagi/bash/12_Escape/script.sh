#!/usr/bin/env bash

# Escaping is a method of quoting single character
# with a certain command and utilites, suck as echo and sed.
#
# Special meaning of certain escaped character:
# \n    means newline.
# \r    means return.
# \t    means tab.
# \v    means vertical tab.
# \b    means backspace.
# \a    means allert.
# \0xx  translate to the octal ASCII equivalent of 0nn.
# \"    gives the quote its literal meaning.
# \$    gives the dollar its literal meaning.
# \\    gives the backslash its literal meaning.

echo ""
echo "This will print
as two lines."
echo "This will print \
as one lines."
echo; echo

echo "========"
echo "\v\v\v\v"
echo "========"
# vertical tab.
echo -e "\v\v\v\v"
echo "========"

echo "Quotation Mark"
echo -e "\042"
echo "=============="

# newline.
echo $'\n'
# maybe beep, depend on terminal.
echo $'\a'

echo $'\t \042 \t'

echo $'\101\102\103'

# literal meaning
echo "\"Hello!\""
