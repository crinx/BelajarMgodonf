#!/bin/bash
# Sharp ( # ) and Bang ( ! ) is indicate command interpreter
# in this case, bash will interpret this script.

# To avoid this possiblity, a script may begin with a `#!/bin/env bash` sha-bang line
# where bash is not located in /bin
echo "Hello World!" || exit 1
