#!/usr/bin/env bash

list="one two three"

# split the variable in parts at whitespace.
for i in $list; do
  echo "$i"
done

echo "----"

# preservers whitespace in a single variable.
for i in "$list"; do
  echo "$i"
done
