#!/usr/bin/env bash

true
echo "exit status of \"true\" is $?"

# need a space between ! and command.
! true
echo "exit status of \"true\" is $?"

# preceding a pipe with ! invert the exit status returned.
ls | random_command
echo $?

# does not change the execution of the pipe.
! ls | random_command
echo $?
