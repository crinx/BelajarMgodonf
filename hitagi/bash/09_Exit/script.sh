#!/usr/bin/env bash

# Exit command terminates a script.
# A successful command returns a 0.
# Non-zero value that usually can be interpreted as an error code.
# Likewise, function whitin a script and the script it self return an exit status.
# Must be an integer 0 - 255.

# When a script end with an exit that has no parameter, 
# the exit status is the exit status of last command executed.

echo "Selamat pagi kurumi >...<"
# $? check return value
echo $?

# unrecognized command.
lskdf
echo $?

echo

exit 133
