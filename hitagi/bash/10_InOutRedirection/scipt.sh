#!/usr/bin/env bash

exit 0

# 1. Overwrite redirection.
# ">" standard output.
# "<" standard input.

cat > file.txt # will copy the input to file.txt.
cat < file.txt # will copy the file.txt to standard output.

# 2. Append redirection.
# ">>" standard output.
# "<<" standard input.
#
# command << delimiter
# do something
# delimiter

cat >> file.txt << "WHAT"
  # will copy the input to file.txt.
  # and then "WHAT" is the delimiter.
WHAT

cat << "EOF" >> file.txt
  # same as above
EOF

# 2. Merge redirection.
# redirect the output of the command to specific file descriptor
# instead of standard output.
# "p >& q" means "merges output from stream p to q".
# "p <& q" means "merges input from stream p to q".
#
# input(0), output(1), error(2)

randomcommandblablabla 2> err.txt # stderr redirection.
neofetch 1> out.txt # stdout redirection.
randomcommand 2>&1 # redirect stderr to stdout.
