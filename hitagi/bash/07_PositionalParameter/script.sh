#!/usr/bin/env bash

# argument passed to the script: $1 $2 $3 $4 . . .
# $0 is the name of the script itself.
# after $9, the argument must be enclosed in bracket: ${10} ${11} ${12}.
# $* and $@ denote all the positional parameters.

MINPARAMS=10

echo "The name of this script is \"$0\"."
echo "The name of this script is \"`basename $0`\"."

if [ -n "$1" ]; then
  echo "Parameter #1 is $1"
fi

if [ -n "$2" ]; then
  echo "Parameter #2 is $2"
fi

if [ -n "$3" ]; then
  echo "Parameter #3 is $3"
fi

if [ -n "$4" ]; then
  echo "Parameter #4 is $4"
fi

if [ -n "$5" ]; then
  echo "Parameter #5 is $5"
fi

if [ -n "$6" ]; then
  echo "Parameter #6 is $6"
fi

if [ -n "$7" ]; then
  echo "Parameter #7 is $7"
fi

if [ -n "$8" ]; then
  echo "Parameter #8 is $8"
fi

if [ -n "$9" ]; then
  echo "Parameter #9 is $9"
fi

if [ -n "${10}" ]; then           # must be enclosed in {bracket}.
  echo "Parameter #10 is ${10}"
fi

echo "=========================================="
echo "All the command-line parameters are: "$*""

if [ $# -lt "$MINPARAMS" ]; then
  echo
  echo "this script need at least $MINPARAMS command-line arguments!"
fi

echo

# total arguments.
args=$#

# indirect reference to $args.
lastarg=${!args} # or lastarg=${!#}.
echo "Last argument is: $lastarg"

# Note that!
# lastarg=${!$#} does not work.

exit 0
