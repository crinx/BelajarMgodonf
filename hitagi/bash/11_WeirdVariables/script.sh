#!/usr/bin/env bash

var="'([\\{}\$\""
echo $var
echo "$var"
echo

IFS='\'
# converted to whitespace.
echo $var
echo "$var"
echo

# illegal
var2="\\\\\""
echo $var2
echo "$var2"
echo

# single quotes to be stricter method of double quotes.
var3='\\\\\'
echo $var3
echo "$var3"
echo

# nesting quotes.
echo "$(echo '"')"

var1="Two bits"
echo "\$var1 = "$var1""
