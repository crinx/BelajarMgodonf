#!/usr/bin/env bash

a=14
b=10

# if statement.
if [ $a -gt $b ]; then
  echo "a is greater than b"
fi

# if else statement.
if [ $a -lt $b ]; then
  echo "a is greater than b"
else
  echo "b is less than a"
fi
