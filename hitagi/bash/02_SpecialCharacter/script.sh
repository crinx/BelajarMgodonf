#!/bin/bash

# 1. Comment
# Lines with a # exception of sha-bang are comment.
echo "Hello World!" # comment at the end of command
echo "The # here does not begin a comment."
echo 'The # here does not begin a comment.'
echo The \# here does not begin a comment.
echo The # here begin a comment.

echo ${PATH#*:}       # Parameter subtitution, not a comment.
echo $(( 2#101011 ))  # Base convertion, not a comment.

# 2. Command Separator
# Permit putting two or more command on the same line.
echo "Command 1"; echo "Command 2"

# 3. Terminator in a case option
variable=xyz

case "$variable" in
  abc) echo "\$variable = abc" ;;
  xyz) echo "\$variable = xyz" ;;
esac
