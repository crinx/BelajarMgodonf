#!/usr/bin/env bash

ls -l [Ss]*
echo

# braketing a string in quotes.
ls -l '[Ss]*'

# grep First or first on any .txt files.
grep '[Ff]irst' *.txt

# one line output, because not indicate as string.
echo $(ls -l)
# for new line, because string will provide new line.
echo "$(ls -l)"
