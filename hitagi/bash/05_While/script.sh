#!/usr/bin/env bash

# looping from true condition until false.
a=2
while [ $a -lt 10 ]; do
  echo $a
  a=`expr $a + 1`
done
