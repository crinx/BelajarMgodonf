#!/usr/bin/env bash

# integer comparison.
#
# -eq = equal to
# -ne = not equal to
# -gt = greater than
# -ge = greater than or equal to
# -lt = less than
# -le = less than or equal to

# string comparison.
#
# == = equal to
# != = not equal to
# <  = less than
# >  = greater than
# -z = zero length
# -n = non zero length
