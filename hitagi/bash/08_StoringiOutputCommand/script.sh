#!/usr/bin/env bash

# store the output of commands inside a variable in a shell script.
var1=$(pwd)
# or
var2=`pwd`

echo "var1: $var1"
echo "var2: $var2"
