#!/usr/bin/env bash

# exit status
E_NOARGS=75

# exit when script without any argument
if [ -z $1 ]; then
  echo "Usage: `basename $0` [hack]"
  exit $E_NOARGS
fi

# multiple invoking script name
case `basename $0` in
  "script.sh" ) echo "Hello";;
  "nasa"      ) echo "Nasa hacked";;
  "google"    ) echo "Google hacked";;
  *           ) echo "NOPE";;
esac

exit $?
