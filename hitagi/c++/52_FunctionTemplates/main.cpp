#include <iostream>
#include <ostream>

template <class T> // declare template
T terbesar(T a, T b){ // single type parameter
	if(a < b){
		return b;
	} else {
		return a;
	}
}

template <class T, class U>
T terkecil(T a, U b){ // multi type parameter
	return(a < b ? a : b);
}

int main(){
	int a = 14, b = 98;
	double c = 41.8, d = 46.2;

	std::cout << terbesar(a,b) << std::endl;
	std::cout << terbesar(c,d) << std::endl;
	
	std::cout << terkecil(a, c) << std::endl;
	std::cout << terkecil(d, b) << std::endl;

	return 0;
}
