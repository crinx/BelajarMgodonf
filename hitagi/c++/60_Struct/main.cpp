#include <iostream>
#include <string>

struct Mahasiswa { // declare struct 
	std::string nama;
	std::string jurusan;
	short int nis;
};

struct { // also declare struct
	std::string nama;
	std::string jurusan;
	short int nis;
} MaBa;

int main(){
	Mahasiswa Baru;

	Baru.nama = "Robert Joe";
	Baru.jurusan = "Teknik Informatika";
	Baru.nis = 9213;

	std::cout << Baru.nama << std::endl;
	std::cout << Baru.jurusan << std::endl;
	std::cout << Baru.nis << std::endl;

	return 0;
}
