#include <iostream>
#include <ostream>

int main(){
	/* pointer is a variable, with the address of another variable as its value */

	/* ampersand operator is address of variable */
	/* asterisk operator is indicating of pointer */

	/* reference (&) */
	/* dereferencee (*) */
	int a = 5;
	int *b = &a;
	int *c;
	c = &a;

	std::cout << &a << std::endl; // use aompersand operator to get address of a variable
	std::cout << *b << std::endl; // use pointer to get value of a variable
	std::cout << c << std::endl; // same address with a variable, because using pointer

	return 0;
}
