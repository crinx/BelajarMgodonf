#include <iostream>
#include <ostream>
#include <string>

class test {
	public:
		void nama(std::string input){
			siswa = input;
		}
		std::string getNama(){ // access specifier
			return siswa;
		}
	private:
		std::string siswa;
};

int main(){
	test a;
	a.nama("Cristina");
	std::cout << a.getNama() << std::endl;
	
	return 0;
}
