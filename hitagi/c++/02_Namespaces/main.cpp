#include <iostream>
#include <ostream>
#include <string>

namespace siswa { // declare namespace
	std::string nama = "Cristina";
	int nis = 231;
}

int main() {
	std::string nama = "Robert";
	int nis = 234;

	std::cout << "Nama: " << nama << std::endl;
	std::cout << "NIS:" << nis << std::endl;

	std::cout << "Nama: " << siswa::nama << std::endl; // siswa:: is namespace siswa
	std::cout << "NIS: " << siswa::nis << std::endl; // use siswa::nis to call variable nis on siswa namespace 
	
	return 0;
}
