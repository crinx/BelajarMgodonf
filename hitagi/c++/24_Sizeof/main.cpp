#include <iostream>
#include <ostream>

int main(){
	int a = 2;
	int b[] = {1, 2, 3};

	std::cout << "Boolean: " << sizeof(bool) << " Byte" << std::endl;
	std::cout << "Char: " << sizeof(char) << " Byte" << std::endl;
	std::cout << "Short: " << sizeof(short int) << " Byte" << std::endl;
	std::cout << "Int: " << sizeof(int) << " Byte" << std::endl;
	std::cout << "Long Int: " << sizeof(long int) << " Byte" << std::endl;
	std::cout << "Long Long: " << sizeof(long long) << " Byte" << std::endl;
	std::cout << "Float: " << sizeof(float) << " Byte" << std::endl;
	std::cout << "Double: " << sizeof(double) << " Byte" << std::endl;
	std::cout << "Long Double: " << sizeof(long double) << " Byte" << std::endl;

	std::cout << sizeof(a) << std::endl;
	std::cout << sizeof(b) << std::endl;
	std::cout << sizeof(b[0]) << std::endl;
	std::cout << sizeof(b) / sizeof(b[0]) << std::endl;

	return 0;
}
