#include <iostream>
#include <string>

template <class T> class Antrian { // declare template class
	private:
		T *antrian;
		int total;
	public:
		Antrian(int ukuran){
			antrian = new T[100];
			total = 0;
		}

		void tambah(T input){
			antrian[total] = input;
			total++;
		}

		void dapatkan(int index){
			std::cout << antrian[index] << std::endl;
		}
};

int main(){
	Antrian<std::string> q(4); // specifier datatype
	Antrian<int> w(2);

	q.tambah("Robert");
	q.tambah("John");
	q.tambah("Cristina");
	w.tambah(2);
	w.tambah(41);

	q.dapatkan(2);
	w.dapatkan(1);

	return 0;
}
