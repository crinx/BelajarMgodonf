#include <iostream>

int main(){
	char a = 'a'; // char variable is typically interpreted as an ASCII character
	
	std::cout << a << std::endl;
  std::cout << "b\n"; // line feed (LF) denotes a transition to a new line.
  std::cout << "c\r"; // carriage return (CR) return to the beginning of the line.
  std::cout << "d\a"; // alarm.
  std::cout << "e\0"; // null. does not return any character.

	return 0;
}
