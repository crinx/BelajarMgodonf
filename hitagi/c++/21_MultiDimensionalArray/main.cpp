#include <iostream>
#include <ostream>

void printArray();

int main(){
	int a[5][5] = {
		{1, 2, 3, 4, 5}, // 1st row
		{1, 2, 3, 4, 5} // 2nd row
	};

	printArray();
	return 0;
}

void printArray(){
	int a[5][5] = {
		{1, 2, 3, 4, 5}, // 1st row
		{1, 2, 3, 4, 5} // 2nd row
	};

	for(int i = 0; i < 5; i++){
		std::cout << a[0][i];
		std::cout << a[1][i];
		std::cout << std::endl;
	}
}
