#include <iostream>
#include <ostream>
#include <string> // using string header to use string utilities

int main(){
	std::string a = "Cristina";
	std::string b = "We are the so-called \"Programmers\" from the north";
	std::string c = "It\'s alright";
	std::string d = "The character \\ is called backslash";
	std::cout << a << std::endl;
	std::cout << b << std::endl;
	std::cout << c << std::endl;
	std::cout << d << std::endl;
	std::cout << a.size() << std::endl; // print length of string
					    
	std::cout << a[0] << std::endl; // access string like array

	return 0;
}
