#include <iostream>
#include <ostream>

class mama {
	public:
		mama(){}
		void hi(){
			std::cout << "Hello" << std::endl;
		}
};

class anak : public mama { // derived from mama class
	public:
		anak(){}
};

class mamaSatunya {
	public:
		mamaSatunya(){}
};

class anakPungut : public mamaSatunya, public mama { // multiple inheritance
};

int main(){
	anak q;
	q.hi(); // all public member mama become public member anak

	return 0;
}
