#include <fstream>
#include <iostream>
#include <ostream>
#include <string>

int main(){
	std::string baris;
	std::ifstream fileGwejh("somefiles.txt"); // can read information from a file using an ifstream or fstream object.

	while(std::getline(fileGwejh, baris)){ // reads characters from an input stream and places them into a string. 
		std::cout << baris << std::endl;
	}

	fileGwejh.close();

	return 0;
}
