#include <iostream>
#include <ostream>
#include "main.h"

myClass::myClass(){

}

void myClass::print(){
	std::cout << "Hello World" << std::endl;
}

int main(){
	myClass q;
	myClass *ptr = &q; // declare pointer to q

	ptr->print(); // access an object's members with a pointer.

	return 0;
}
