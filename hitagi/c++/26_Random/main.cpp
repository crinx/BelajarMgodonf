#include <cstdlib>
#include <ctime>
#include <iostream>
#include <ostream>

void acak();
void acakV2();

int main(){
	for(int i = 0; i < 5; i++){
		std::cout << rand() << std::endl;
	}

	int a =  1 + (rand() % 6); // random number 1 - 6.

	std::cout << a << std::endl;
//	acak();
	acakV2();

	return 0;
}

void acak(){
	srand(97);

	for(int i = 5; i > 0; i--){
		std::cout << 1 + (rand() % 5) << std::endl;
	}
}

void acakV2(){
	srand(time(0)); // time(0) will return the current second count

	for(int i = 0; i < 5; i++){
		std::cout << 1 + (rand() % 6) << std::endl;
	}
}
