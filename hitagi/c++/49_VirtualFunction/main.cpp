#include <iostream>
#include <string>

class hewan {
	public:
		std::string nama;
		virtual void berbicara(){ // look like template
			std::cout << "Gwejh adalah hewan!" << std::endl;
		}
};

class anjing : public hewan {
	public:
		void berbicara(){ // confirm it have function
			std::cout << "Woof! Woof!" << std::endl;
		}
};

class herobrine : public hewan { // use default virtual function
};

int main(){
	anjing a1;
	herobrine h1;

	a1.nama = "Slamet";
	h1.nama = "Steve";

	hewan *q1 = &a1;
	hewan *q2 = &h1;

	hewan *arr[] = {q1, q2};
	for(int i = 0; i < 2; i++){
		arr[i] -> berbicara();
	}

	return 0;
}
