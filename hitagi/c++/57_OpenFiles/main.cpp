#include <iostream>
#include <fstream>
#include <ostream>

int main(){
	std::fstream fileGwejh;
	fileGwejh.open("somefiles.txt", std::ios::binary | std::ios::in | std::ios::out | std::ios::app); // open mode.
													  /* ios::app append to end of file
													   * ios::ate go to end of file on opening
													   * ios::binary file open in binary mode
													   * ios::in open file for reading only
													   * ios::out open file for writing only
													   * ios::trunc delete the contents of the file if it exsist
													   * */

	if(fileGwejh.is_open()){
		fileGwejh << "Hello World!";
	} else {
		std::cout << "Something went wrong" << std::endl;
	}

	fileGwejh.close();
	
	return 0;
}
