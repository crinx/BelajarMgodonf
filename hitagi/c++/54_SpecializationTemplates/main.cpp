#include <iostream>

template <class T>
class kelasGwejh {
	public:
		kelasGwejh(T x){
			std::cout << x << " - is not a char" << std::endl;
		}
};

template < > // should be like this 
class kelasGwejh<char> { // specialization parameter
	public:
		kelasGwejh(char x){
			std::cout << x << " - is a char!" << std::endl;
		}
};

int main(){
	kelasGwejh<int> q(54);
	kelasGwejh<double> w(4.1);
	kelasGwejh<char> e('4');

	return 0;
}
