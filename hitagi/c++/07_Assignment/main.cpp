#include <iostream>
#include <ostream>

int main(){
	int a = 1, b = 2;
	
	std::cout << a << std::endl;
	a += b; // a = a + b
	std::cout << a << std::endl;
	a -= b; // a = a - b
	std::cout << a << std::endl;

	return 0;
}
