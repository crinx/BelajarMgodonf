#include <iostream>
#include <ostream>
#include "main.h"

kelasGwejh::kelasGwejh(int a)
: var(a)
{
}

void kelasGwejh::printIngfo(){
	std::cout << var << std::endl;
	std::cout << this -> var << std::endl; // select to member variable
	std::cout << (*this).var << std::endl; // is a pointer to the object
}

int main(){
	kelasGwejh q(21);
	q.printIngfo();

	return 0;
}
