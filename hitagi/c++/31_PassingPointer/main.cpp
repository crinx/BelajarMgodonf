#include <iostream>
#include <ostream>

void print(int input);
void printC(int *input);

int main(){
	int a = 2;

	print(a); // pass by value
	printC(&a); // pass by reference
	return 0;
}

void print(int input){
	std::cout << input << std::endl;
}

void printC(int *input){
	std::cout << *input << std::endl;
}
