#include <iostream>
#include <ostream>

int totalArray();

int main(){
	int a[5] = {12, 23, 51, 92, 9};
	
	for(int i = 0; i < 5; i++){
		std::cout << a[i] << std::endl;
	}

	std::cout << a[5] << std::endl; // out of index

	a[3] = 3; // replace value
	std::cout << a[3] << std::endl;

	std::cout << totalArray() << std::endl;

	return 0;
}

int totalArray(){
	int a[] = {1, 2, 3, 4, 5};
	int total = 0;

	for(int i = 0; i < 5; i++){
		total += a[i];
	}

	return total;
}
