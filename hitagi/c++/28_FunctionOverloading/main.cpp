#include <iostream>
#include <ostream>

/* same function name, but different on the parameter */
void print(int a){
	std::cout << a << std::endl;
}

void print(double a){
	std::cout << a << std::endl;
}

int main(){
	print(12);
	print(3.14);

	return 0;
}
