#include <iostream>

class Queue {
	int size;
	int *queue;

	public:
	Queue(){
		size = 0;
		queue = new int[100];
	}

	void add(int data){
		queue[size] = data;
		size++;
	}

	void remove(){
		if(size == 0){
			std::cout << "Queue is empty" << std::endl;
			return;
		} else {
			for(int i = 0; i < size; i++){
				queue[i] = queue[i + 1];
			}
			size--;
		}
	}

	void print(){
		if(size == 0){
			std::cout << "Queue is empty" << std::endl;
			return;
		} else {
			for(int i = 0; i < size; i++){
				std::cout << queue[i] << " <-";
			}
			std::cout << std::endl;
		}
	}
	Queue operator+(Queue &lama){
		Queue baru;
		baru.size = this -> size + lama.size;
		
		for(int i = 0; i < baru.size; i++){
			if(i < size){
				baru.queue[i] = this -> queue[i];
			}

			if(i >= size){
				baru.queue[i] = this -> queue[i] + lama.queue[i - size];
			}
		}

		return baru;
	}
};

int main(){
	Queue q1;
	q1.add(42); q1.add(2); q1.add(8); q1.add(1); q1.add(92);
	Queue q2;
	q2.add(3); q2.add(66); q2.add(128); q2.add(5);
	Queue q3 = q1 + q2;
	q3.print();

	return 0;
}
