#include <iostream>

int main(){
	try {
		int umurMama = 29;
		int umurAnak = 30;
		if(umurMama < umurAnak){
			throw 99;
		}
	} catch(int x){ // only handle int type
		std::cout << "Kesalahan pada isi umur! - Error" << x << std::endl;
	}

	try {
		std::string menu[] = {"Pisang", "Kelapa", "Tomat", "Ikan"};
		int input;
		std::cin >> input;

		if(input > 3){
			throw "OUTOFRANGE";
		} else {
			std::cout << menu[input] << std::endl;
		}
	} catch(...){ // ellipsis operator can handle any type
		std::cout << "Menu tidak tersedia, mohon coba lagi" << std::endl;
	}

	return 0;
}
