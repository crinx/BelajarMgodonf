#include <iostream>
#include <utility>

class Antrian {
	protected:
		int ukuran;
		int *antrian;
	public:
		Antrian(){
			ukuran = 0;
			antrian = new int[50];
		}

		void tambah(int data){
			antrian[ukuran] = data;
			ukuran++;
		}

		void hapus(){
			if(ukuran == 0){
				std::cout << "Antrian sedang kosong" << std::endl;
				return;
			} else {
				for(int i = 0; i < ukuran - 1; i++){
					antrian[i] = antrian[i + 1];
				}
				ukuran--;
			}
		}

		virtual void print(){
			if(ukuran == 0){
				std::cout << "Antrian sedang kosong" << std::endl;
				return;
			}

			for(int i = 0; i < ukuran; i++){
				std::cout << antrian[i] << " <- ";
			}

			std::cout << std::endl;
		}

		Antrian operator+(Antrian &input){
			Antrian q;
			for(int i = 0; i < this -> ukuran; i++){
				q.tambah(this -> antrian[i]);
			}
			
			for(int i = 0; i < input.ukuran; i++){
				q.tambah(input.antrian[i]);
			}

			return q;
		}
};

class Antrian2 : public Antrian {
	public:
		void print(){
			if(ukuran ==0){
				std::cout << "Antrian sedang kosong" << std::endl;
				return;
			}

			for(int i = 0; i < ukuran; i++){
				std::cout << antrian[i] << std::endl;
				std::cout << " ^ " << std::endl;
			}

			std::cout << std::endl;
		}
};

int main(){
	Antrian q;
	Antrian2 w;

	q.tambah(123); q.tambah(87); q.tambah(85); q.tambah(2);
	w.tambah(76); w.tambah(81); w.tambah(21); w.tambah(19);

	q.print();
	w.print();

	return 0;
}
