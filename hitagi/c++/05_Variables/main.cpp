#include <iostream>
#include <ostream>

int main(){
	const int a = 10; // constant variable, cannot change this value
	int b = 21; // regular variable
	auto c = 21.2; // auto variable, auto select variable data type.
	
	std::cout << a << std::endl;
	std::cout << b << std::endl;
	std::cout << c << std::endl;

	b = 32;
	c = 76.1;
	std::cout << b << std::endl;
	std::cout << c << std::endl;

	return 0;
}
