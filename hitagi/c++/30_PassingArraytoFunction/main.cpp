#include <cstdlib>
#include <ctime>
#include <iostream>
#include <ostream>

int tiket(int input[], int size = 5);

int main(){
	int peserta[5];

	for(int i = 0; i < 5; i++){
		std::cin >> peserta[i];
	}

	std::cout << "----- =" << std::endl;
	std::cout << "Rp " << tiket(peserta, 5) << "00" << std::endl;
	std::cout << "Total harga tiket yang harus dibayar" << std::endl;

	return 0;
}

int tiket(int input[], int size){
	int hargaTiket = 0;
	srand(time(0));
	
	for(int i = 0; i < size; i++){
		hargaTiket += input[i] * (1 + (rand() % 5));
	}
	
	return hargaTiket;
}
