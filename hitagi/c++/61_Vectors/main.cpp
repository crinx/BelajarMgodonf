#include <iostream>
#include <vector>

int main() {
  // vector initialization
  // 1 dimension
  std::vector<int>angka(5);
  // 2 dimension
  std::vector<std::vector<int>> angkaAngka(5, std::vector<int>(7));
  // 3 dimension
  std::vector<std::vector<std::vector<int>>> angkaAngkaAngka(3, std::vector<std::vector<int>>(15, std::vector<int>(20)));
  // 4 dimension
  std::vector<std::vector<std::vector<std::vector<int>>>> bilangan(1, std::vector<std::vector<std::vector<int>>>(2, std::vector<std::vector<int>>(3, std::vector<int>(4))));

  std::vector<int>var;

  // print size of array.
  std::cout << var.size() << std::endl;
  
  // expand array size.
  var.push_back(9); // 9 is a value, not the total number of indexes
  std::cout << var.size() << std::endl;
  var.push_back(0);
  std::cout << var.size() << std::endl;

  return 0;
}
