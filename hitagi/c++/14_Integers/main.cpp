#include <iostream>

int main(){
	int a = 12;

	/* integers have some modifiers type */
	signed int b = -21; // can hold positive and negative value
	unsigned int c = 31; // only hold positive value
	short int d = 23; // half of the default size
	long int e = 3127915413278; // twice the default size
	unsigned long f = 1523912386745;
  int g = 300'000'000; // value in a more legible
  int h = 3e8; // mean three exponent eight.

	std::cout << h << std::endl;

	return 0;
}
