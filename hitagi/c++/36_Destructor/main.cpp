#include "main.h"

myClass::myClass()
{
	std::cout << "Constructor" << std::endl; // called when start
}

myClass::~myClass()
{
	std::cout << "Destructor" << std::endl; // called when finish
}

int main(){
	myClass q;	

	return 0;
}
