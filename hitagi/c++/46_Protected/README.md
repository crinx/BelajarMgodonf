# Access Specifier

Access specifier used to specify the **type of inheritance**

**Public inheritance:** > Public member from base class become public member on derived class, protected member from base class become protected member on derived class.

**Protected inheritance:** > Public and protected member from base class become protected member on derived class.

**Private inheritance:** > Public and protected member from base class become private member on derived class.
