#include <iostream>
#include <string>

class produk {
	protected: // like private but can acesssed in the derived classes
		double harga;
		int bobot;
	public:
		void ingfo(){
			std::cout << harga << ", " << bobot;
		}
};

class buah : public produk {
	public:
		std::string type;
		void setIngfo(double h, int b){
			harga = h;
			bobot = b;
		}
};

int main(){
	buah q;
	q.type = "Jagung";
	q.setIngfo(4.99, 90);
	q.ingfo();

	return 0;
}
