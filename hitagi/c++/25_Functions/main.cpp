#include <iostream>
#include <ostream>

// function declaration
int total(int a, int b);
void jumlah(int a, int b);

int main(){
	int a = total(12, 23);
	std::cout << a << std::endl;
	jumlah(12, 23 /* input parameter */);

	return 0;
}

// function definition
int total(int a, int b){
	return a + b; // function with return statement
}

void jumlah(int a, int b /* function parameter */){
	std::cout << a + b << std::endl; // function without return value
}
