#include <iostream>
#include <ostream>

int main(){
	int a = 1;

	if(a < 5){
		std::cout << "a lebih kecil dari 5" << std::endl;
	}else if(a > 5){
		std::cout << "a lebih besar dari 5" << std::endl;
	}else{
		std::cout << "nilai a tidak diketahui" << std::endl;
	}

	return 0;
}
