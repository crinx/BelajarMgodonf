#include "main.h"

myClass::myClass() // myClass constructor in header file
{
// 1.  scope resolution operator is used to define particular class member
};

int main(){
	myClass q;

	return 0;
}
