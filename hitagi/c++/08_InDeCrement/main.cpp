#include <iostream>
#include <ostream>

int main(){
	int a = 1, c, d;
	std::cout << a << std::endl;

	c = ++a; // prefix
	std::cout << a << std::endl;
	std::cout << c << std::endl;

	d = a++; // postfix
	std::cout << a << std::endl;
	std::cout << d << std::endl;

	return 0;
}
