#include <iostream>
#include <ostream>

int total(int a = 1, int b = 1); // default argument

int main(){
	std::cout << total(2) << std::endl;

	return 0;
}

int total(int a, int b){
	return a + b;
}
