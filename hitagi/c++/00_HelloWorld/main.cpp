#include <iostream>

int main() {
  std::cout << "Hello World!" << std::endl;
  return 0;
}

/* cout : display text to the console.
 * <<   : insertion operator.
 * >>   : extraction operator.
 * endl : end of line.*/

