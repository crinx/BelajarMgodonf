#include <iostream>
#include <ostream>

int main(){
	/* There are three different floating point data types:
	 * float, double, and long double */
	float a = 12.2; // 4 byte
	double b = 14.21; // 7 byte
	long double c = 123.412; // can be equivalent to a double (8 bytes), or 16 bytes
	/* floating data type are always signed */

	std::cout << a << std::endl;
	std::cout << b << std::endl;
	std::cout << c << std::endl;

	return 0;
}
