#include <iostream>

class musuh {
	protected:
		int kekuatanMenyerang;
	public:
		void setKekuatanMenyerang(int a){
			kekuatanMenyerang = a;
		}
};

class ninja : public musuh {
	public:
		void menyerang(){
			std::cout << "Ninja! - " << kekuatanMenyerang << std::endl;
		}
};

class monster : public musuh {
	public:
		void menyerang(){
			std::cout << "Monster! - " << kekuatanMenyerang << std::endl;
		}
};

int main(){
	ninja q;
	monster w;

	musuh *e1 = &q;
	musuh *e2 = &w;

	e1 -> setKekuatanMenyerang(20);
	e2 -> setKekuatanMenyerang(90);

	q.menyerang();
	w.menyerang();

	return 0;
}
