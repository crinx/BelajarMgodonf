#include <string>

#ifndef MAIN_H
#define MAIN_H

class ultah {
	public:
		ultah(int d, int m, int y);
		void printTanggal();
	private:
		int hari, bulan, tahun;
	
};

class orang {
	public:
		orang(std::string a, ultah b);
		void printIngfo();
	private:
		std::string nama;
		ultah ut; // use another class as member
};

#endif
