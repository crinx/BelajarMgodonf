#include <iostream>
#include <ostream>
#include "main.h"

ultah::ultah(int d, int m, int y)
: hari(d), bulan(m), tahun(y)
{
}

orang::orang(std::string a, ultah b)
: nama(a), ut(b)
{
}

void ultah::printTanggal(){
	std::cout << hari << "/" << bulan << "/";
	std::cout << tahun << std::endl;
}

void orang::printIngfo(){
	std::cout << nama << std::endl;
	ut.printTanggal();
}

int main(){
	ultah q(15, 4, 2003);
	orang t("Robert", q);
	t.printIngfo();

	return 0;
}
