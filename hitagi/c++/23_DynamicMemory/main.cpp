#include <cstdio>
#include <iostream>
#include <ostream>

int main(){
	int *ptr = new int; /* pointer ptr is stored in the stack as a local variable, 
			       and holds the heap allocated address as its value*/
	*ptr = 5;

	std::cout << *ptr << std::endl; // use the value
	delete ptr; // free up the memory
	// now ptr is a dangling pointer
	ptr = new int; // reuse for a new address

	int *newPtr = NULL; // declare pointer with NULL
	newPtr = new int[5]; // request memory
	delete [] newPtr; // delete array pointed to by newPtr

	return 0;
}
