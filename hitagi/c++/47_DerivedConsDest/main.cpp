#include <iostream>

class mama {
	public:
		mama(){
			std::cout << "mama ctor" << std::endl;
		}

		~mama(){
			std::cout << "mama dtor" << std::endl;
		}
};

class anak : public mama {
	public:
		anak(){
			std::cout << "anak ctor" << std::endl;
		}

		~anak(){
			std::cout << "anak dtor" << std::endl;
		}
};

int main(){
	anak w;

	return 0;
}
