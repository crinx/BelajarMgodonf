#include <iostream>
#include <ostream>
#include <string>

class myClass{
	public:
		myClass(std::string x){ // constructor name is same as class name
			std::cout << "Ini adalah kelas gwejh" << std::endl;
			setNama(x);
			// constructor is function are executed when class declarated
		}
		void setNama(std::string input){
			nama = input;
		}
		std::string getNama(){
			return nama;
		}
	private:
		std::string nama;
};

int main(){
	myClass kelasGwejh("Robert");
	std::cout << kelasGwejh.getNama() << std::endl;
	kelasGwejh.setNama("Cristina"); // will be repleaced by new input
	std::cout << kelasGwejh.getNama() << std::endl;

	return 0;
}
