#include <iostream>
#include <fstream>

int main(){
	std::ofstream fileGwejh;
	std::ofstream fileLuwh("otherfiles.txt"); // use ofstream constructor instead.
	fileGwejh.open("somefiles.txt");

	fileGwejh << "Hello World";
	
	if(fileLuwh.is_open()){ // is_open function checks whether the file is open and ready.
		fileLuwh << "Hello Server";
	} else {
		std::cout << "Something went wrong";
	}

	fileGwejh.close(); // close when done.
	fileLuwh.close();

	return 0;
}
