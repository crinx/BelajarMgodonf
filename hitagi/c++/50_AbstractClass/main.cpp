#include <iostream>

class hewan {
	public:
		virtual void berbicara() = 0; // declare pure virtual class
};

class anjing : public hewan {
	public:
		void berbicara(){
			std::cout << "Woof! Woof!" << std::endl;
		}
};

int main(){
	anjing q;

	hewan *w1 = &q;

	w1 -> berbicara();

	return 0;
}
