#ifndef MAIN_H
#define MAIN_H

class point {
	public:
		point(int a, int b);
		void print();
	private:
		int x, y;
		friend void shift(point &p, int step); // can modify whole of point function, included private member
};

#endif
