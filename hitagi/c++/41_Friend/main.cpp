#include <iostream>
#include <ostream>
#include "main.h"

point::point(int a, int b)
: x(a), y(b)
{
}

void point::print(){
	std::cout << x << ", " << y;
}

void shift(point &p, int step){
	p.x += step;
	p.y += step;
}

int main(){
	int x, y;
	int step;

	std::cin >> x >> y;
	std::cin >> step;

	point p(x, y);
	shift(p, step);

	p.print();

	return 0;
}
