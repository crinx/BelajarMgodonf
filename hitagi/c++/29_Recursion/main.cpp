#include <iostream>
#include <ostream>

int factorial(int n = 1);

int main(){
	std::cout << factorial(5) << std::endl;

	return 0;
}

int factorial(int n){
	if (n == 1){
		return 1;
	}else{
		return n * factorial(n -1);
	}
}
