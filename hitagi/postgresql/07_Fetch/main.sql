SELECT
	film_id, title
FROM
	film
ORDER BY
	title ASC
FETCH
	FIRST 1 ROW ONLY;
-----------------------------------
SELECT
	film_id, title
FROM
	film
ORDER BY
	title ASC
FETCH
	FIRST 5 ROW ONLY;
-----------------------------------
SELECT
	film_id, title
FROM
	film
ORDER BY
	title ASC
OFFSET
	5 ROW
FETCH
	FIRST 5 ROW ONLY;
