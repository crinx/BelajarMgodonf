SELECT
	review, title
FROM
	films
RIGHT JOIN film_reviews
	ON film_reviews.film_id = films.film_id;
---------------------------------------------------
SELECT
	review, title
FROM
	films
RIGHT JOIN film_reviews USING(film_id);
---------------------------------------------------
-- To find the rows from the right table that does not have any corresponding rows in the left table
SELECT
	review, title
FROM
	films
RIGHT JOIN film_reviews USING(film_id)
WHERE title IS NULL;
