SELECT
	film.film_id,
	title,
	inventory_id
FROM
	film
LEFT JOIN inventory
	ON inventory.film_id = film.film_id
ORDER BY
	title ASC;
---------------------------------------------
-- find NULL in inventory_id
SELECT
	film.film_id,
	title,
	inventory_id
FROM
	film
LEFT JOIN inventory
	ON inventory.film_id = film.film_id
WHERE inventory_id IS NULL
ORDER BY
	title ASC;
--------------------------------------------
-- find NULL in inventory_id with TABLE ALIASES
SELECT
	f.film_id,
	title,
	inventory_id
FROM
	film AS f
LEFT JOIN inventory AS i
	ON i.film_id = f.film_id
WHERE i.film_id IS NULL
ORDER BY
	title ASC;
--------------------------------------------
-- find NULL in inventory_id with USING syntax
SELECT
	f.film_id,
	title,
	inventory_id
FROM
	film AS f
LEFT JOIN inventory AS i USING (film_id)
WHERE i.film_id IS NULL
ORDER BY
	title ASC;
