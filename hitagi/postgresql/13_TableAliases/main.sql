SELECT
	c.customer_id, first_name, amount, payment_date
FROM
	customer c
INNER JOIN payment p
	ON p.customer_id = c.customer_id
ORDER BY
	payment_date DESC;
-----------------------------------------------
-- self join
SELECT
	e.first_name AS employee, m.first_name AS manager
FROM
	employee e
INNER JOIN employee m
	ON m.employee_id = e.manager_id
ORDER BY
	manager ASC;
