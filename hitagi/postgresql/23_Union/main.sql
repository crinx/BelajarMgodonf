-- simple example
SELECT * FROM top_rated_films
UNION
SELECT * FROM most_popular_films;
---------------------------
-- combine result set wih UNION ALL
SELECT * FROM top_rated_films
UNION ALL
SELECT * FROM most_popular_films;
---------------------------
-- with order by clause
SELECT * FROM top_rated_films
UNION ALL
SELECT * FROM most_popular_films
ORDER BY title ASC;
