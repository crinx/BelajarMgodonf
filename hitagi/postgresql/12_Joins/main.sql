-- inner join
SELECT
	a, fruit_a, b, fruit_b
FROM
	basket_a
INNER JOIN basket_b
	ON fruit_a = fruit_b;
------------------------------------
-- left join
SELECT
	a, fruit_a, b, fruit_b
FROM
	basket_a
LEFT JOIN basket_b
	ON fruit_a = fruit_b;
------------------------------------
-- outher left join
SELECT
	a, fruit_a, b, fruit_b
FROM
	basket_a
LEFT JOIN basket_b
	ON fruit_a = fruit_b
WHERE b IS NULL;
------------------------------------
-- right join
SELECT
	a, fruit_a, b, fruit_b
FROM
	basket_a
RIGHT JOIN basket_b
	ON fruit_a = fruit_b;
------------------------------------
-- right outer join
SELECT
	a, fruit_a, b, fruit_b
FROM
	basket_a
RIGHT JOIN basket_b
	ON fruit_a = fruit_b
WHERE a IS NULL;
-----------------------------------
-- full outer join
SELECT
	a, fruit_a, b, fruit_b
FROM
	basket_a
FULL OUTER JOIN basket_b
	ON fruit_a = fruit_b;
-----------------------------------
-- full outer join - only unique value
SELECT
	a, fruit_a, b, fruit_b
FROM
	basket_a
FULL JOIN basket_b
	ON fruit_a = fruit_b
WHERE a IS NULL OR b IS NULL;

