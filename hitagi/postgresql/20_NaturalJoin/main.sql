-- to JOIN products with the categories
SELECT * FROM products
NATURAL JOIN categories;
--------------------------------------------
-- equal result with INNER JOIN
SELECT * FROM products
INNER JOIN categories USING(category_id);
--------------------------------------------
SELECT * FROM city
NATURAL JOIN country;
