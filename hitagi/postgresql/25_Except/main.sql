-- return distinct rows from the first and second query
SELECT * FROM top_rated_films
EXCEPT
SELECT * FROM most_popular_films
ORDER BY title ASC;
