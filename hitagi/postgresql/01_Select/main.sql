SELECT
	first_name -- select single column
FROM
	customer;
---------------------------------------------------------------
SELECT
	first_name, last_name, email -- multi select column

FROM
	customer;
---------------------------------------------------------------
SELECT
	* -- select all column
FROM
	customer;
---------------------------------------------------------------
SELECT
	first_name || ' ' || last_name -- return full name
FROM
	customer;
---------------------------------------------------------------
SELECT 5 * 3; -- return expression
