SELECT
	id,
	bcolor,
	fcolor

FROM distinct_demo;
-------------------------------------------
SELECT
	DISTINCT bcolor -- distinct single column
FROM
	distinct_demo
ORDER BY
	bcolor ASC;
-------------------------------------------
SELECT
	DISTINCT bcolor, fcolor -- distinct multiple column
FROM
	distinct_demo
ORDER BY
	bcolor ASC, fcolor ASC;
-------------------------------------------
SELECT
	DISTINCT ON (bcolor) bcolor, fcolor -- distinct on statement
FROM
	distinct_demo
ORDER BY
	bcolor ASC, fcolor ASC;
