SELECT
	employee_name, department_name
FROM
	employees AS e
FULL OUTER JOIN departments AS d
	ON d.department_id = e.department_id;
-------------------------------------------------
-- to find departments not have any employees
SELECT
	employee_name, department_name
FROM
	employees AS e
FULL OUTER JOIN departments AS d
	ON d.department_id = e.department_id
WHERE
	employee_name IS NULL;
-------------------------------------------------
-- to find employee who not belong to any departments
SELECT
	employee_name, department_name
FROM
	employees AS e
FULL OUTER JOIN departments AS d
	ON d.department_id = e.department_id
WHERE
	department_name IS NULL;
