SELECT
	brand, segment, SUM(quantity)
FROM
	sales
GROUP BY
	ROLLUP(segment, brand)
ORDER BY
	segment, brand;
--------------------------------------
SELECT
	brand, segment, SUM(quantity)
FROM
	sales
GROUP BY
	segment, ROLLUP(brand) -- hierarchy segment > brand.
ORDER BY
	segment, brand;
--------------------------------------
SELECT
	EXTRACT(YEAR FROM rental_date) AS y,
	EXTRACT(MONTH FROM rental_date) AS M,
	EXTRACT(DAY FROM rental_date) AS d,
	COUNT(rental_id)
FROM
	rental
GROUP BY
	ROLLUP(
		EXTRACT(YEAR FROM rental_date),
		EXTRACT(MONTH FROM rental_date),
		EXTRACT(DAY FROM rental_date)
	);
