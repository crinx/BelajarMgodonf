SELECT
	brand, segment, SUM(quantity)
FROM
	sales
GROUP BY
	brand, segment;
--------------------------------------
SELECT
	brand, SUM(quantity)
FROM
	sales
GROUP BY
	brand;
--------------------------------------
SELECT
	segment, SUM(quantity)
FROM
	sales
GROUP BY
	segment;
--------------------------------------
SELECT
	SUM(quantity)
FROM
	sales;
--------------------------------------
-- multiple grouping set (bad performance)
SELECT
	brand, segment, SUM(quantity)
FROM
	sales
GROUP BY
	brand, segment
UNION ALL
SELECT
	brand, NULL, SUM(quantity)
FROM
	sales
GROUP BY
	brand
UNION ALL
SELECT
	NULL, segment, SUM(quantity)
FROM
	sales
GROUP BY
	segment
UNION ALL
SELECT
	NULL, NULL, SUM(quantity)
FROM
	sales;
--------------------------------------
-- multiple grouping set (optimized)
SELECT
	brand, segment, SUM(quantity)
FROM
	sales
GROUP BY
	GROUPING SETS(
		(brand, segment),
		(brand),
		(segment),
		()
	);
--------------------------------------
SELECT
	GROUPING(brand) grouping_brand,
	GROUPING(segment) grouping_segment,
	brand,
	segment,
	SUM(quantity)
FROM
	sales
GROUP BY
	GROUPING SETS(
		(brand),
		(segment),
		()
	)
ORDER BY
	brand,
	segment;
--------------------------------------
-- finding subtotal of each brand
SELECT
	GROUPING(brand) grouping_brand,
	GROUPING(segment) grouping_segment,
	brand,
	segment,
	SUM(quantity)
FROM
	sales
GROUP BY
	GROUPING SETS(
		(brand),
		(segment),
		()
	)
HAVING GROUPING(brand) = 0
ORDER BY
	brand,
	segment;
