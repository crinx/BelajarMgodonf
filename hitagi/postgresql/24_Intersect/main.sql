-- returns any row that are avaible in both result set
SELECT *
FROM most_popular_films
INTERSECT
SELECT *
FROM top_rated_films;
