-- without aggregate function
SELECT
	customer_id
FROM
	payment
GROUP BY
	customer_id;
-------------------------
-- with sum function
SELECT
	customer_id,
	SUM(amount)
FROM
	payment
GROUP BY 
	customer_id
ORDER BY
	SUM(amount) DESC;
-------------------------
-- with join clause
SELECT
	first_name || ' '|| last_name AS full_name,
	SUM(amount) AS amount
FROM
	payment
INNER JOIN customer USING(customer_id)
GROUP BY
	full_name
ORDER BY amount DESC;
------------------------
-- with cout function
SELECT
	staff_id,
	COUNT(payment_id)
FROM
	payment
GROUP BY
	staff_id;
------------------------
-- multiple column
SELECT
	customer_id, staff_id,
	SUM(amount)
FROM
	payment
GROUP BY
	staff_id, customer_id
ORDER BY
	customer_id;
------------------------
-- with date column
SELECT
	DATE(payment_date) AS paid_date,
	SUM(amount) AS sum
FROM
	payment
GROUP BY
	DATE(payment_date);
