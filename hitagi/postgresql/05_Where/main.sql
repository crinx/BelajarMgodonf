SELECT
	first_name, last_name
FROM
	customer
WHERE
	first_name = 'Jamie'; -- equal operator
---------------------------------------------------
SELECT
	first_name, last_name
FROM
	customer
WHERE
	first_name = 'Jamie' AND last_name = 'Rice'; -- AND operator
---------------------------------------------------
SELECT
	first_name, last_name
FROM
	customer
WHERE
	first_name = 'Adam' OR last_name = 'Rodriguez'; -- OR operator
---------------------------------------------------
SELECT
	first_name, last_name
FROM
	customer
WHERE
	first_name IN ('Ann', 'Anne', 'Annie'); -- IN operator
---------------------------------------------------
SELECT
	first_name, last_name
FROM
	customer
WHERE
	first_name LIKE 'Ann%'; -- LIKE operator
---------------------------------------------------
SELECT
	first_name, LENGTH(first_name) name_length
FROM
	customer
WHERE
	-- BETWEEN operator
	first_name LIKE 'A%' AND LENGTH(first_name) BETWEEN 3 AND 5
ORDER BY
	name_length;
---------------------------------------------------
SELECT
	first_name, last_name
FROM
	customer
WHERE
	-- not equal
	first_name LIKE 'Bra%' AND last_name <> 'Motley';
