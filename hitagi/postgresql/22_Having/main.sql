-- with SUM function
SELECT
	customer_id,
	SUM(amount)
FROM
	payment
GROUP BY
	customer_id
HAVING
	SUM(amount) > 200;
------------------------------
-- with COUNT function
SELECT
	store_id,
	COUNT(customer_id)
FROM
	customer
GROUP BY
	store_id
HAVING
	COUNT(customer_id) > 300;
