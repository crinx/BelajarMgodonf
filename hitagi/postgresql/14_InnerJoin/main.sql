-- two table
SELECT
	customer.customer_id, first_name, last_name, amount, payment_date
FROM
	customer
INNER JOIN payment
	ON payment.customer_id = customer.customer_id
ORDER BY
	payment_date ASC;
------------------------------------------------------------
-- use table aliases
SELECT
	c.customer_id, first_name, last_name, email, amount, payment_date
FROM
	customer AS c
INNER JOIN payment as p
	ON p.customer_id = c.customer_id
WHERE
	c.customer_id = 2;
------------------------------------------------------------
-- use using syntax
SELECT
	customer_id, first_name, last_name, amount, payment_date
FROM
	customer
INNER JOIN payment USING(customer_id)
ORDER BY
	payment_date ASC;
------------------------------------------------------------
-- three table
SELECT
	c.customer_id, c.first_name AS customer_first_name, c.last_name AS customer_last_name,
	s.first_name AS staff_first_name, s.last_name AS staff_last_name,
	amount, payment_date
FROM
	customer AS c
INNER JOIN payment AS p
	ON p.customer_id = c.customer_id
INNER JOIN staff AS s
	ON p.staff_id = s.staff_id
ORDER BY
	payment_date ASC;
