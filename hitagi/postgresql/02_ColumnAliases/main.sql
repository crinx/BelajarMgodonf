SELECT
	first_name AS nama_depan -- column aliases
FROM
	customer;
--------------------------------------------------------
SELECT
	first_name || ' ' || last_name AS full_name -- assigning a column alias to an expression
FROM
	customer;
--------------------------------------------------------
SELECT
	last_name AS "nama belakang" -- column aliases that contain spaces
FROM
	customer;
