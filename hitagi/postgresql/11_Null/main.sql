SELECT
	id, first_name, last_name, email, phone
FROM
	contacts
WHERE
	phone IS NULL;
-------------------------------------------------
SELECT
	id, first_name, last_name, email, phone
FROM
	contacts
WHERE
	phone IS NOT NULL;
