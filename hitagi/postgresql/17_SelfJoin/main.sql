SELECT
	e.first_name || ' ' || e.last_name AS employee,
	m.first_name || ' ' || m.last_name AS manager
FROM
	employee AS e
INNER JOIN employee AS m ON m .employee_id = e.manager_id
ORDER BY
	manager ASC;
------------------------------------------------------------
SELECT
	f1.title, f2.title, f1.length
FROM
	film AS f1
INNER JOIN film f2
	ON f1.film_id <> f2.film_id
	AND f1.length = f2.length;
