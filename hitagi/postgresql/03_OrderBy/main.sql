SELECT
	first_name
FROM
	customer
ORDER BY
	first_name ASC; -- sort row in ascending order
---------------------------------
SELECT
	last_name
FROM
	customer
ORDER BY
	last_name DESC; -- sort row in descending order
---------------------------------
SELECT
	first_name, last_name
FROM
	customer
ORDER BY
	first_name ASC, last_name ASC; -- sort multiple row
---------------------------------
SELECT
	first_name,
	LENGTH(first_name) len
FROM
	customer
ORDER BY
	len DESC; -- sort row by expressions
---------------------------------
-- display data
SELECT
	num
FROM
	sort_demo
ORDER BY
	num;
---------------------------------
SELECT
	num
FROM
	sort_demo
ORDER BY
	num NULLS FIRST; -- null in first order
--------------------------------
SELECT
	num
FROM
	sort_demo
ORDER BY
	num NULLS LAST; -- null in last order
