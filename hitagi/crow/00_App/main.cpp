#include <crow.h>
#include <crow/app.h>

int main(int argc, char *argv[])
{
  // initalize app has no middlewares.
  crow::SimpleApp app;

  // configure app and run.
  app.port(8080)
    .multithreaded()
    .run();
}
