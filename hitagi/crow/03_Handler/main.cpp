#include <crow.h>
#include <crow/app.h>
#include <crow/common.h>
#include <crow/http_request.h>
#include <crow/http_response.h>

// reference of documentation: https://crowcpp.org/master/reference/structcrow_1_1response.html

int main(int argc, char *argv[])
{
  crow::SimpleApp app;

  CROW_ROUTE(app, "/").methods(crow::HTTPMethod::GET)
  ([](crow::request& req){ // request handler.
    req.url_params.get(nullptr); // acccess the url parameter.
  });

  CROW_ROUTE(app, "/").methods(crow::HTTPMethod::POST)
  ([](crow::response& res){ // response handler.
    return crow::response(); // to return response defined as a parameter.
  });

  app.port(8080)
    .multithreaded()
    .run();
}
