#include <crow.h>
#include <crow/app.h>
#include <crow/common.h>

int main(int argc, char *argv[])
{
  crow::SimpleApp app;

  // http method reqest configuration.
  CROW_ROUTE(app, "/").methods(crow::HTTPMethod::GET)
  ([](){
    return "GET methods";
  });

  CROW_ROUTE(app, "/").methods(crow::HTTPMethod::PUT)
  ([](){
    return "PUT methods";
  });

  CROW_ROUTE(app, "/").methods(crow::HTTPMethod::POST)
  ([](){
    return "POST methods";
  });

  CROW_ROUTE(app, "/").methods(crow::HTTPMethod::DELETE)
  ([](){
    return "DELETE methods";
  });

  app.port(8080)
    .multithreaded()
    .run();
}
