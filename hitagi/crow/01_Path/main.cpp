#include <crow.h>
#include <crow/app.h>
#include <string>

int main(int argc, char *argv[])
{
  crow::SimpleApp app;

  // PATH configuration, parameter can be int, uint, double, string or path.
  CROW_ROUTE(app, "/<int>/<int>")
  ([](int a, int b)
  {
    return std::to_string(a + b);
  });

  app.port(8080)
    .multithreaded()
    .run();
}
